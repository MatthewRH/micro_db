SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION dbo.udf_MPANcheck
(@MPAN varchar(13))
RETURNS BIT
AS
BEGIN
DECLARE @Isok bit
DECLARE @prime int;
DECLARE @total int;
DECLARE @mpan_pos int;
DECLARE @mpanloc varchar(13);
set @mpanloc = @MPAN

DECLARE @Primes TABLE (VALUE INTEGER)
--CREATE TABLE #Primes
--(p INTEGER NOT NULL PRIMARY KEY)

insert into @Primes values(3)  
insert into @Primes values(5)  
insert into @Primes values(7)  
insert into @Primes values(13)  
insert into @Primes values(17)  
insert into @Primes values(19)  
insert into @Primes values(23)  
insert into @Primes values(29)  
insert into @Primes values(31)  
insert into @Primes values(37)  
insert into @Primes values(41)  
insert into @Primes values(43)  


declare primes_crs CURSOR
for select * from @Primes

OPEN primes_crs
FETCH NEXT FROM primes_crs INTO @prime

set @total = 1;
set @mpan_pos = 0;
WHILE @@FETCH_STATUS = 0
BEGIN
	set @total = @total  + (substring(@mpanloc,@mpan_pos,1) * @prime)
	--print substring(@mpanloc,@mpan_pos,1) + ' ' + cast(@total as varchar) + ' prime = ' + cast(@prime as varchar)
	set @mpan_pos += 1;
	FETCH NEXT FROM primes_crs INTO @prime
	
END

CLOSE primes_crs
DEALLOCATE primes_crs

iF (CAST((@total%11)%10 AS VARCHAR) = substring(@mpanloc,13,1))
Set @Isok = 1
ELSE
Set @Isok = 0
--print @Isok
RETURN @Isok

END
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION levelisation_rates_fn ()
RETURNS @LevTab TABLE  (Tariff_Code nvarchar(255),
						price2010 Numeric(12,2),
						price2011 Numeric(12,2),
						price2012 Numeric(12,2),
						price2013 Numeric(12,2), 
						price2014 Numeric(12,2), 
						price2015 Numeric(12,2))

AS 
BEGIN

-- Add the SELECT statement with parameter references here
--IF OBJECT_ID('tempdb..#Price_1') IS NOT NULL DROP TABLE #Price_1
--IF OBJECT_ID('tempdb..#Price_2') IS NOT NULL DROP TABLE #Price_2
--IF OBJECT_ID('tempdb..#Price_3') IS NOT NULL DROP TABLE #Price_3
--IF OBJECT_ID('tempdb..#Price_4') IS NOT NULL DROP TABLE #Price_4
--IF OBJECT_ID('tempdb..#Price_5') IS NOT NULL DROP TABLE #Price_5
--IF OBJECT_ID('tempdb..#Price_6') IS NOT NULL DROP TABLE #Price_6


DECLARE @Price_1 AS TABLE
	(Tariff_Code NVARCHAR(255),
	Apr2010_Price Numeric(12,2)
	)

DECLARE @Price_2 AS TABLE
	(Tariff_Code NVARCHAR(255),
	Apr2011_Price Numeric(12,2)
	)

DECLARE @Price_3 AS TABLE
	(Tariff_Code NVARCHAR(255),
	Apr2012_Price Numeric(12,2)
	)

DECLARE @Price_4 AS TABLE
	(Tariff_Code NVARCHAR(255),
	Apr2013_Price Numeric(12,2)
	)

DECLARE @Price_5 AS TABLE
	(Tariff_Code NVARCHAR(255),
	Apr2014_Price Numeric(12,2)
	)

DECLARE @Price_6 AS TABLE
	(Tariff_Code NVARCHAR(255),
	Apr2015_Price Numeric(12,2)
	)


INSERT INTO @Price_1
SELECT 
Tariff_Code,
Price 
FROM Rates
WHERE Valid_From Between '01Apr2010' AND '31Mar2011'


INSERT INTO @Price_2
SELECT 
Tariff_Code,
Price 
FROM Rates
WHERE Valid_From Between '01Apr2011' AND '31Mar2012'


INSERT INTO @Price_3
SELECT 
Tariff_Code,
Price 
FROM Rates
WHERE Valid_From Between '01Apr2012' AND '31Mar2013'

INSERT INTO @Price_4
SELECT 
Tariff_Code,
Price 
FROM Rates
WHERE Valid_From Between '01Apr2013' AND '31Mar2014'

INSERT INTO @Price_5
SELECT 
Tariff_Code,
Price 
FROM Rates
WHERE Valid_From Between '01Apr2014' AND '31Mar2015'

INSERT INTO @Price_6
SELECT 
Tariff_Code,
Price 
FROM Rates
WHERE Valid_From >= '01Apr2015'


INSERT INTO @LevTab
SELECT DISTINCT
R.Tariff_Code,
Apr2010_Price,
Apr2011_Price,
Apr2012_Price,
Apr2013_Price,
Apr2014_Price,
Apr2015_Price

FROM Rates r
LEFT JOIN @Price_1 p1
On r.Tariff_Code = p1.Tariff_Code

LEFT JOIN @Price_2 p2
On r.Tariff_Code = p2.Tariff_Code

LEFT JOIN @Price_3 p3
On r.Tariff_Code = p3.Tariff_Code

LEFT JOIN @Price_4 p4
On r.Tariff_Code = p4.Tariff_Code

LEFT JOIN @Price_5 p5
On r.Tariff_Code = p5.Tariff_Code

LEFT JOIN @Price_6 p6
On r.Tariff_Code = p6.Tariff_Code

--ORDER BY r.Tariff_Code
RETURN


END
GO

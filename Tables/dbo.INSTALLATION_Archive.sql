SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[INSTALLATION_Archive] (
		[INSTALLATION_ID]                       [int] NOT NULL,
		[Scheme_Type]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Supply_MPAN]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Export_MPAN]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Export_Status]                         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Address_1]                             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Address_2]                             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[City]                                  [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[County]                                [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Postal_Code]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[OS_Grid_Ref]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Type_of_Installation]                  [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Generation_Type]                       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Generation_Capacity]                   [int] NULL,
		[Generation_MSN]                        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Export_MSN]                            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Total_Installed_Capacity_kW]           [float] NULL,
		[Total_Declared_Net_Capacity_kW]        [float] NULL,
		[Estimated_Annual_Generation]           [int] NULL,
		[RO_Accreditation_Number]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Make_of_Generation_Equipment]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Model_of_Generation_Equipment]         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Installation_Name]                     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Generation_Meter_Make_and_Model]       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Agent_Appointment_Letter_Received]     [bit] NULL,
		[LOA_Received]                          [bit] NULL,
		[Electrical_Schematic_Received]         [bit] NULL,
		[EPC_Reference]                         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Application_Received_Date]             [datetime] NULL,
		[FiT_ID_Initial]                        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[MCS_Certification_Number_1]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Initial_System_Tariff_Rate]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Initial_System_Export_Rate]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Initial_System_Pct_Split]              [float] NULL,
		[Initial_System_Start_Date]             [datetime] NULL,
		[Initial_System_End_Date]               [datetime] NULL,
		[Initial_System_Commissioning_Date]     [datetime] NULL,
		[FiT_ID_Ext1]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[MCS_Certification_Number_2]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Extension_1_Tariff_Rate]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Extension_1_Export_Rate]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Extension_1_Pct_Split]                 [float] NULL,
		[Extension_1_Start_Date]                [datetime] NULL,
		[Extension_1_End_Date]                  [datetime] NULL,
		[Extension_1_Commissioning_Date]        [datetime] NULL,
		[FiT_ID_Ext2]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[MCS_Certification_Number_3]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Extension_2_Tariff_Rate]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Extension_2_Export_Rate]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Extension_2_Pct_Split]                 [float] NULL,
		[Extension_2_Start_Date]                [datetime] NULL,
		[Extension_2_End_Date]                  [datetime] NULL,
		[Extension_2_Commissioning_Date]        [datetime] NULL,
		[TopUp_Tariff_Rate]                     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Modified_By]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Date_Modified]                         [datetime] NULL,
		[Time_Modified]                         [datetime] NULL,
		[Cos_Flag]                              [bit] NULL,
		[SysDesc]                               [nvarchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ExpPct1]                               [float] NULL,
		[ExpPct2]                               [float] NULL,
		[InitSysExpRate2]                       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[FiT_Related]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[DC]                                    [nvarchar](4) COLLATE Latin1_General_CI_AS NULL,
		[Created_By]                            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Created_Date]                          [date] NULL,
		[Created_Time]                          [time](7) NULL,
		[Export_Meter_Photo]                    [bit] NULL,
		[Confirmation_Date]                     [datetime] NULL,
		[Ext1_Confirmation_Date]                [datetime] NULL,
		[Ext2_Confirmation_Date]                [datetime] NULL,
		[Modified_by2]                          [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Created_by2]                           [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Created_date2]                         [datetime] NULL,
		[Modified_date2]                        [datetime] NULL,
		[Remotely_Read_Enabled_From]            [datetime] NULL,
		[Remotely_Read_Enabled]                 [bit] NULL,
		[Epc_Date]                              [datetime] NULL,
		[Valid_From]                            [datetime] NULL,
		[Initial_Meter_Reading]                 [decimal](8, 2) NULL,
		[Initial_Meter_Read_Date]               [datetime] NULL,
		[Originating_Client_Reference]          [varchar](45) COLLATE Latin1_General_CI_AS NULL,
		[Generation_Meter_Model]                [nvarchar](100) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[INSTALLATION_Archive]
	ADD
	CONSTRAINT [INSTALLATION_Archive$PrimaryKey]
	PRIMARY KEY
	CLUSTERED
	([INSTALLATION_ID])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[INSTALLATION_Archive]
	ADD
	CONSTRAINT [DF__INSTALLAT__Schem__38B96646]
	DEFAULT ('FiT') FOR [Scheme_Type]
GO
ALTER TABLE [dbo].[INSTALLATION_Archive]
	ADD
	CONSTRAINT [DF__INSTALLAT__Agent__39AD8A7F]
	DEFAULT ((0)) FOR [Agent_Appointment_Letter_Received]
GO
ALTER TABLE [dbo].[INSTALLATION_Archive]
	ADD
	CONSTRAINT [DF__INSTALLAT__LOA_R__3AA1AEB8]
	DEFAULT ((0)) FOR [LOA_Received]
GO
ALTER TABLE [dbo].[INSTALLATION_Archive]
	ADD
	CONSTRAINT [DF__INSTALLAT__Elect__3B95D2F1]
	DEFAULT ((0)) FOR [Electrical_Schematic_Received]
GO
ALTER TABLE [dbo].[INSTALLATION_Archive]
	ADD
	CONSTRAINT [DF__INSTALLAT__Initi__3C89F72A]
	DEFAULT ((1)) FOR [Initial_System_Pct_Split]
GO
ALTER TABLE [dbo].[INSTALLATION_Archive]
	ADD
	CONSTRAINT [DF__INSTALLAT__Exten__3D7E1B63]
	DEFAULT ((0)) FOR [Extension_1_Pct_Split]
GO
ALTER TABLE [dbo].[INSTALLATION_Archive]
	ADD
	CONSTRAINT [DF__INSTALLAT__Exten__3E723F9C]
	DEFAULT ((0)) FOR [Extension_2_Pct_Split]
GO
ALTER TABLE [dbo].[INSTALLATION_Archive]
	ADD
	CONSTRAINT [DF__INSTALLAT__Creat__3F6663D5]
	DEFAULT (getdate()) FOR [Created_Date]
GO
ALTER TABLE [dbo].[INSTALLATION_Archive]
	ADD
	CONSTRAINT [DF__INSTALLAT__Creat__405A880E]
	DEFAULT (CONVERT([time],getdate(),(0))) FOR [Created_Time]
GO
ALTER TABLE [dbo].[INSTALLATION_Archive]
	ADD
	CONSTRAINT [DF__INSTALLAT__Modif__414EAC47]
	DEFAULT (suser_name()) FOR [Modified_by2]
GO
ALTER TABLE [dbo].[INSTALLATION_Archive]
	ADD
	CONSTRAINT [DF__INSTALLAT__Creat__4242D080]
	DEFAULT (suser_name()) FOR [Created_by2]
GO
ALTER TABLE [dbo].[INSTALLATION_Archive]
	ADD
	CONSTRAINT [DF__INSTALLAT__Creat__4336F4B9]
	DEFAULT (getdate()) FOR [Created_date2]
GO
ALTER TABLE [dbo].[INSTALLATION_Archive]
	ADD
	CONSTRAINT [DF__INSTALLAT__Modif__442B18F2]
	DEFAULT (getdate()) FOR [Modified_date2]
GO
ALTER TABLE [dbo].[INSTALLATION_Archive] SET (LOCK_ESCALATION = TABLE)
GO

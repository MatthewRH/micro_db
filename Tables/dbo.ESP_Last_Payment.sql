SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ESP_Last_Payment] (
		[SAP_CA]                  [float] NULL,
		[Last_Payment_Date]       [datetime] NULL,
		[Last_Payment_Amount]     [float] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ESP_Last_Payment] SET (LOCK_ESCALATION = TABLE)
GO

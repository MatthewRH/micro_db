SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CONTRACT_Archive] (
		[CONTRACT_ID]                            [int] NOT NULL,
		[BP_ID_link]                             [int] NOT NULL,
		[Account_Status]                         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Payment_Method]                         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Bank_Acc_Name]                          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Sort_Code]                              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Bank_Acc_Number]                        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Bank_Reference]                         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[SAP_BP]                                 [int] NULL,
		[SAP_CA]                                 [int] NULL,
		[SAP_Contract]                           [int] NULL,
		[Terms_and_Conditions_Sent_Out_Date]     [datetime] NULL,
		[Terms_and_Conditions_Agreed_Date]       [datetime] NULL,
		[Terms_and_Conditions_Agreed]            [bit] NULL,
		[Proof_of_Ownership_Received]            [bit] NULL,
		[Date_of_Last_Reaffirmation]             [datetime] NULL,
		[RO_Transfer]                            [bit] NULL,
		[Final_bill_paid]                        [bit] NULL,
		[Grant_Received]                         [bit] NULL,
		[Grant_Form_Completed]                   [bit] NULL,
		[Third_Party]                            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Third_Party_Ref]                        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_Company]                             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_Title]                               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_First_Name]                          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_Last_Name]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_Business_Phone]                      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_Home_Phone]                          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_Mobile_Phone]                        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_Email_Address]                       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_Address_1]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_Address_2]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_City]                                [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_County]                              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_Postal_Code]                         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[INSTALLATION_ID_link]                   [int] NULL,
		[Move_In]                                [datetime] NULL,
		[Move_Out]                               [datetime] NULL,
		[IMPORTANT]                              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Modified_By]                            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Date_Modified]                          [datetime] NULL,
		[Time_Modified]                          [datetime] NULL,
		[T_C_Sent_Out_Date_Ext1]                 [datetime] NULL,
		[T_C_Agreed_Date_Ext1]                   [datetime] NULL,
		[T_C_Sent_Out_Date_Ext2]                 [datetime] NULL,
		[T_C_Agreed_Date_Ext2]                   [datetime] NULL,
		[MultiSiteNR]                            [bit] NULL,
		[Vat_Inv_Rqd]                            [bit] NULL,
		[Created_By]                             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Created_Date]                           [date] NULL,
		[Created_Time]                           [time](7) NULL,
		[modified_by2]                           [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[created_by2]                            [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[created_date2]                          [datetime] NULL,
		[modified_date2]                         [datetime] NULL,
		[Postcoder_Errors]                       [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[Microtricity_Errors]                    [varchar](1000) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CONTRACT_Archive]
	ADD
	CONSTRAINT [CONTRACT_Archive$PrimaryKey]
	PRIMARY KEY
	CLUSTERED
	([CONTRACT_ID])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[CONTRACT_Archive]
	ADD
	CONSTRAINT [DF__CONTRACT___Terms__4707859D]
	DEFAULT ((0)) FOR [Terms_and_Conditions_Agreed]
GO
ALTER TABLE [dbo].[CONTRACT_Archive]
	ADD
	CONSTRAINT [DF__CONTRACT___Proof__47FBA9D6]
	DEFAULT ((0)) FOR [Proof_of_Ownership_Received]
GO
ALTER TABLE [dbo].[CONTRACT_Archive]
	ADD
	CONSTRAINT [DF__CONTRACT___RO_Tr__48EFCE0F]
	DEFAULT ((0)) FOR [RO_Transfer]
GO
ALTER TABLE [dbo].[CONTRACT_Archive]
	ADD
	CONSTRAINT [DF__CONTRACT___Final__49E3F248]
	DEFAULT ((0)) FOR [Final_bill_paid]
GO
ALTER TABLE [dbo].[CONTRACT_Archive]
	ADD
	CONSTRAINT [DF__CONTRACT___Grant__4AD81681]
	DEFAULT ((0)) FOR [Grant_Received]
GO
ALTER TABLE [dbo].[CONTRACT_Archive]
	ADD
	CONSTRAINT [DF__CONTRACT___Grant__4BCC3ABA]
	DEFAULT ((0)) FOR [Grant_Form_Completed]
GO
ALTER TABLE [dbo].[CONTRACT_Archive]
	ADD
	CONSTRAINT [DF__CONTRACT___Creat__4CC05EF3]
	DEFAULT (getdate()) FOR [Created_Date]
GO
ALTER TABLE [dbo].[CONTRACT_Archive]
	ADD
	CONSTRAINT [DF__CONTRACT___Creat__4DB4832C]
	DEFAULT (CONVERT([time],getdate(),(0))) FOR [Created_Time]
GO
ALTER TABLE [dbo].[CONTRACT_Archive]
	ADD
	CONSTRAINT [DF__CONTRACT___modif__4EA8A765]
	DEFAULT (suser_name()) FOR [modified_by2]
GO
ALTER TABLE [dbo].[CONTRACT_Archive]
	ADD
	CONSTRAINT [DF__CONTRACT___creat__4F9CCB9E]
	DEFAULT (suser_name()) FOR [created_by2]
GO
ALTER TABLE [dbo].[CONTRACT_Archive]
	ADD
	CONSTRAINT [DF__CONTRACT___creat__5090EFD7]
	DEFAULT (getdate()) FOR [created_date2]
GO
ALTER TABLE [dbo].[CONTRACT_Archive]
	ADD
	CONSTRAINT [DF__CONTRACT___modif__51851410]
	DEFAULT (getdate()) FOR [modified_date2]
GO
ALTER TABLE [dbo].[CONTRACT_Archive] SET (LOCK_ESCALATION = TABLE)
GO

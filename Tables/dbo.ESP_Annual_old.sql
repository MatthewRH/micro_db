SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ESP_Annual_old] (
		[Installation_ID_link]             [int] NULL,
		[Contract_Number]                  [nvarchar](30) COLLATE Latin1_General_CI_AS NULL,
		[Net_Suite_Site_ID]                [int] NULL,
		[Year]                             [int] NULL,
		[PPA_Payment]                      [float] NULL,
		[PPA_Anticipated_Output]           [float] NULL,
		[PPA_Guaranteed_Annual_Output]     [float] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ESP_Annual_old] SET (LOCK_ESCALATION = TABLE)
GO

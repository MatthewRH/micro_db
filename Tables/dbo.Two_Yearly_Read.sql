SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Two_Yearly_Read] (
		[id_number]                [int] IDENTITY(1, 1) NOT NULL,
		[Generation_Read]          [int] NULL,
		[Actual_Read_Date]         [datetime] NULL,
		[Scheduled_Visit_Date]     [datetime] NULL,
		[NoRead_Reason]            [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Agent_Notes]              [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Action_Required]          [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Photo_of_Meter]           [bit] NOT NULL,
		[General_Notes]            [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Installation_ID]          [int] NULL,
		[Modified_By]              [varchar](250) COLLATE Latin1_General_CI_AS NULL,
		[Date_Modified]            [datetime] NULL,
		[Time_Modified]            [time](7) NULL,
		[Agent]                    [varchar](15) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Two_Yearly_Read]
	ADD
	CONSTRAINT [PK__Two_Year__D58CDE104A18FC72]
	PRIMARY KEY
	CLUSTERED
	([id_number])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Two_Yearly_Read]
	ADD
	CONSTRAINT [DF_Two_Yearly_Read_Photo_of_Meter]
	DEFAULT ((0)) FOR [Photo_of_Meter]
GO
GRANT ALTER
	ON [dbo].[Two_Yearly_Read]
	TO [admin_role]
GO
GRANT DELETE
	ON [dbo].[Two_Yearly_Read]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[Two_Yearly_Read]
	TO [admin_role]
GO
GRANT SELECT
	ON [dbo].[Two_Yearly_Read]
	TO [admin_role]
GO
GRANT UPDATE
	ON [dbo].[Two_Yearly_Read]
	TO [admin_role]
GO
GRANT VIEW DEFINITION
	ON [dbo].[Two_Yearly_Read]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[Two_Yearly_Read]
	TO [userRole]
GO
GRANT SELECT
	ON [dbo].[Two_Yearly_Read]
	TO [userRole]
GO
GRANT UPDATE
	ON [dbo].[Two_Yearly_Read]
	TO [userRole]
GO
ALTER TABLE [dbo].[Two_Yearly_Read] SET (LOCK_ESCALATION = TABLE)
GO

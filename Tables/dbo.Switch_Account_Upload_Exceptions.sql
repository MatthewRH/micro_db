SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Switch_Account_Upload_Exceptions] (
		[GeneratorID]                          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Third_Party]                          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Third_Party_Ref]                      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Terms_and_Conditions_Sent_Date]       [datetime] NULL,
		[Terms_and_Conditions_Agreed_Date]     [datetime] NULL,
		[Account_Status]                       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[VAT_Invoice_Self_Billing]             [bit] NULL,
		[installation_id_link]                 [int] NULL,
		[Move_Out]                             [datetime] NULL,
		[Proof_of_Ownership_Received]          [bit] NULL,
		[Payment_Method]                       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[VAT_Inv_Rqd]                          [bit] NULL,
		[fit_id_initial]                       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Type_of_Installation]                 [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Generation_Type]                      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Export_Status]                        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Generation_Meter_Make_and_Model]      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Generation_Meter_Model]               [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Remotely_Read_Enabled]                [bit] NULL,
		[Address_1]                            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Address_2]                            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[City]                                 [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Postal_Code]                          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Initial_System_Export_Rate]           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Original_FIT_ID]                      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Check_Length]                         [int] NULL,
		[Count_Dupes]                          [int] NULL,
		[Switch_ID]                            [varchar](50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
GRANT DELETE
	ON [dbo].[Switch_Account_Upload_Exceptions]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[Switch_Account_Upload_Exceptions]
	TO [admin_role]
GO
GRANT SELECT
	ON [dbo].[Switch_Account_Upload_Exceptions]
	TO [admin_role]
GO
GRANT UPDATE
	ON [dbo].[Switch_Account_Upload_Exceptions]
	TO [admin_role]
GO
GRANT VIEW DEFINITION
	ON [dbo].[Switch_Account_Upload_Exceptions]
	TO [admin_role]
GO
GRANT VIEW CHANGE TRACKING
	ON [dbo].[Switch_Account_Upload_Exceptions]
	TO [admin_role]
GO
GRANT DELETE
	ON [dbo].[Switch_Account_Upload_Exceptions]
	TO [userRole]
GO
GRANT INSERT
	ON [dbo].[Switch_Account_Upload_Exceptions]
	TO [userRole]
GO
GRANT SELECT
	ON [dbo].[Switch_Account_Upload_Exceptions]
	TO [userRole]
GO
GRANT UPDATE
	ON [dbo].[Switch_Account_Upload_Exceptions]
	TO [userRole]
GO
GRANT VIEW DEFINITION
	ON [dbo].[Switch_Account_Upload_Exceptions]
	TO [userRole]
GO
GRANT VIEW CHANGE TRACKING
	ON [dbo].[Switch_Account_Upload_Exceptions]
	TO [userRole]
GO
ALTER TABLE [dbo].[Switch_Account_Upload_Exceptions] SET (LOCK_ESCALATION = TABLE)
GO

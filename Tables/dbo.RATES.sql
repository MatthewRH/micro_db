SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RATES] (
		[RATE_ID]                      [int] IDENTITY(1, 1) NOT NULL,
		[Generation_Type]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Tariff_Code]                  [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Description]                  [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[FiT_Period]                   [int] NULL,
		[Min_Capacity]                 [int] NULL,
		[Max_Capacity]                 [int] NULL,
		[Valid_From]                   [datetime] NULL,
		[Valid_To]                     [datetime] NULL,
		[Price]                        [numeric](12, 2) NULL,
		[Modified_By]                  [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Date_Modified]                [datetime] NULL,
		[Time_Modified]                [datetime] NULL,
		[EligibilityDateFrom]          [datetime] NULL,
		[EligibilityDateTo]            [datetime] NULL,
		[Modified_by2]                 [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Created_by2]                  [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Created_date2]                [datetime] NULL,
		[Modified_date2]               [datetime] NULL,
		[ESP_Year]                     [int] NULL,
		[Gross_Electricity_Charge]     [numeric](12, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RATES]
	ADD
	CONSTRAINT [RATES$PrimaryKey]
	PRIMARY KEY
	CLUSTERED
	([RATE_ID])
	ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[RATES].[PrimaryKey]', 'SCHEMA', N'dbo', 'TABLE', N'RATES', 'CONSTRAINT', N'RATES$PrimaryKey'
GO
ALTER TABLE [dbo].[RATES]
	ADD
	CONSTRAINT [DF__RATES__Modified___7CA47C3F]
	DEFAULT (suser_name()) FOR [Modified_by2]
GO
ALTER TABLE [dbo].[RATES]
	ADD
	CONSTRAINT [DF__RATES__Created_b__7D98A078]
	DEFAULT (suser_name()) FOR [Created_by2]
GO
ALTER TABLE [dbo].[RATES]
	ADD
	CONSTRAINT [DF__RATES__Created_d__7E8CC4B1]
	DEFAULT (getdate()) FOR [Created_date2]
GO
ALTER TABLE [dbo].[RATES]
	ADD
	CONSTRAINT [DF__RATES__Modified___7F80E8EA]
	DEFAULT (getdate()) FOR [Modified_date2]
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[RATES]', 'SCHEMA', N'dbo', 'TABLE', N'RATES', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[RATES].[RATE_ID]', 'SCHEMA', N'dbo', 'TABLE', N'RATES', 'COLUMN', N'RATE_ID'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[RATES].[Generation_Type]', 'SCHEMA', N'dbo', 'TABLE', N'RATES', 'COLUMN', N'Generation_Type'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[RATES].[Tariff_Code]', 'SCHEMA', N'dbo', 'TABLE', N'RATES', 'COLUMN', N'Tariff_Code'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[RATES].[Description]', 'SCHEMA', N'dbo', 'TABLE', N'RATES', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[RATES].[FiT_Period]', 'SCHEMA', N'dbo', 'TABLE', N'RATES', 'COLUMN', N'FiT_Period'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[RATES].[Min_Capacity]', 'SCHEMA', N'dbo', 'TABLE', N'RATES', 'COLUMN', N'Min_Capacity'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[RATES].[Max_Capacity]', 'SCHEMA', N'dbo', 'TABLE', N'RATES', 'COLUMN', N'Max_Capacity'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[RATES].[Valid_From]', 'SCHEMA', N'dbo', 'TABLE', N'RATES', 'COLUMN', N'Valid_From'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[RATES].[Valid_To]', 'SCHEMA', N'dbo', 'TABLE', N'RATES', 'COLUMN', N'Valid_To'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[RATES].[Price]', 'SCHEMA', N'dbo', 'TABLE', N'RATES', 'COLUMN', N'Price'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[RATES].[Modified_By]', 'SCHEMA', N'dbo', 'TABLE', N'RATES', 'COLUMN', N'Modified_By'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[RATES].[Date_Modified]', 'SCHEMA', N'dbo', 'TABLE', N'RATES', 'COLUMN', N'Date_Modified'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[RATES].[Time_Modified]', 'SCHEMA', N'dbo', 'TABLE', N'RATES', 'COLUMN', N'Time_Modified'
GO
GRANT ALTER
	ON [dbo].[RATES]
	TO [admin_role]
GO
GRANT DELETE
	ON [dbo].[RATES]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[RATES]
	TO [admin_role]
GO
GRANT SELECT
	ON [dbo].[RATES]
	TO [admin_role]
GO
GRANT UPDATE
	ON [dbo].[RATES]
	TO [admin_role]
GO
GRANT VIEW DEFINITION
	ON [dbo].[RATES]
	TO [admin_role]
GO
GRANT SELECT
	ON [dbo].[RATES]
	TO [userRole]
GO
ALTER TABLE [dbo].[RATES] SET (LOCK_ESCALATION = TABLE)
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UK_Postcodes] (
		[Postcode]     [varchar](5) COLLATE Latin1_General_CI_AS NOT NULL,
		[Town]         [varchar](255) COLLATE Latin1_General_CI_AS NOT NULL,
		[County]       [varchar](255) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UK_Postcodes]
	ADD
	CONSTRAINT [PK_UK_Postcodes]
	PRIMARY KEY
	CLUSTERED
	([Postcode])
	ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'Database Data Source', N'http://www.ukpostcodes.org/postcode-database-api-downloads', 'SCHEMA', N'dbo', 'TABLE', N'UK_Postcodes', NULL, NULL
GO
GRANT ALTER
	ON [dbo].[UK_Postcodes]
	TO [admin_role]
GO
GRANT DELETE
	ON [dbo].[UK_Postcodes]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[UK_Postcodes]
	TO [admin_role]
GO
GRANT SELECT
	ON [dbo].[UK_Postcodes]
	TO [admin_role]
GO
GRANT UPDATE
	ON [dbo].[UK_Postcodes]
	TO [admin_role]
GO
GRANT VIEW DEFINITION
	ON [dbo].[UK_Postcodes]
	TO [admin_role]
GO
GRANT VIEW CHANGE TRACKING
	ON [dbo].[UK_Postcodes]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[UK_Postcodes]
	TO [userRole]
GO
GRANT SELECT
	ON [dbo].[UK_Postcodes]
	TO [userRole]
GO
GRANT UPDATE
	ON [dbo].[UK_Postcodes]
	TO [userRole]
GO
ALTER TABLE [dbo].[UK_Postcodes] SET (LOCK_ESCALATION = TABLE)
GO

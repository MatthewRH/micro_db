SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CONTRACT] (
		[CONTRACT_ID]                            [int] IDENTITY(1, 1) NOT NULL,
		[BP_ID_link]                             [int] NOT NULL,
		[Account_Status]                         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Payment_Method]                         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Bank_Acc_Name]                          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Sort_Code]                              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Bank_Acc_Number]                        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Bank_Reference]                         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[SAP_BP]                                 [int] NULL,
		[SAP_CA]                                 [int] NULL,
		[SAP_Contract]                           [int] NULL,
		[Terms_and_Conditions_Sent_Out_Date]     [datetime] NULL,
		[Terms_and_Conditions_Agreed_Date]       [datetime] NULL,
		[Terms_and_Conditions_Agreed]            [bit] NULL,
		[Proof_of_Ownership_Received]            [bit] NULL,
		[Date_of_Last_Reaffirmation]             [datetime] NULL,
		[RO_Transfer]                            [bit] NULL,
		[Final_bill_paid]                        [bit] NULL,
		[Grant_Received]                         [bit] NULL,
		[Grant_Form_Completed]                   [bit] NULL,
		[Third_Party]                            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Third_Party_Ref]                        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_Company]                             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_Title]                               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_First_Name]                          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_Last_Name]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_Business_Phone]                      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_Home_Phone]                          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_Mobile_Phone]                        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_Email_Address]                       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_Address_1]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_Address_2]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_City]                                [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_County]                              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[NR_Postal_Code]                         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[INSTALLATION_ID_link]                   [int] NULL,
		[Move_In]                                [datetime] NULL,
		[Move_Out]                               [datetime] NULL,
		[IMPORTANT]                              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Modified_By]                            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Date_Modified]                          [datetime] NULL,
		[Time_Modified]                          [datetime] NULL,
		[T_C_Sent_Out_Date_Ext1]                 [datetime] NULL,
		[T_C_Agreed_Date_Ext1]                   [datetime] NULL,
		[T_C_Sent_Out_Date_Ext2]                 [datetime] NULL,
		[T_C_Agreed_Date_Ext2]                   [datetime] NULL,
		[MultiSiteNR]                            [bit] NULL,
		[Vat_Inv_Rqd]                            [bit] NULL,
		[Created_By]                             [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Created_Date]                           [date] NULL,
		[Created_Time]                           [time](7) NULL,
		[modified_by2]                           [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[created_by2]                            [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[created_date2]                          [datetime] NULL,
		[modified_date2]                         [datetime] NULL,
		[SSMA_TimeStamp]                         [timestamp] NOT NULL,
		[Postcoder_Errors]                       [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[Microtricity_Errors]                    [varchar](1000) COLLATE Latin1_General_CI_AS NULL,
		[ESP_ID]                                 [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[VAT_Invoice_Manual]                     [bit] NULL,
		[VAT_Invoice_Self_Billing]               [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CONTRACT]
	ADD
	CONSTRAINT [CONTRACT$PrimaryKey]
	PRIMARY KEY
	CLUSTERED
	([CONTRACT_ID])
	ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[PrimaryKey]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'CONSTRAINT', N'CONTRACT$PrimaryKey'
GO
ALTER TABLE [dbo].[CONTRACT]
	ADD
	CONSTRAINT [DF__CONTRACT__Terms___056ECC6A]
	DEFAULT ((0)) FOR [Terms_and_Conditions_Agreed]
GO
ALTER TABLE [dbo].[CONTRACT]
	ADD
	CONSTRAINT [DF__CONTRACT__Proof___0662F0A3]
	DEFAULT ((0)) FOR [Proof_of_Ownership_Received]
GO
ALTER TABLE [dbo].[CONTRACT]
	ADD
	CONSTRAINT [DF__CONTRACT__RO_Tra__075714DC]
	DEFAULT ((0)) FOR [RO_Transfer]
GO
ALTER TABLE [dbo].[CONTRACT]
	ADD
	CONSTRAINT [DF__CONTRACT__Final___084B3915]
	DEFAULT ((0)) FOR [Final_bill_paid]
GO
ALTER TABLE [dbo].[CONTRACT]
	ADD
	CONSTRAINT [DF__CONTRACT__Grant___093F5D4E]
	DEFAULT ((0)) FOR [Grant_Received]
GO
ALTER TABLE [dbo].[CONTRACT]
	ADD
	CONSTRAINT [DF__CONTRACT__Grant___0A338187]
	DEFAULT ((0)) FOR [Grant_Form_Completed]
GO
ALTER TABLE [dbo].[CONTRACT]
	ADD
	CONSTRAINT [DF__CONTRACT__Create__4FD1D5C8]
	DEFAULT (getdate()) FOR [Created_Date]
GO
ALTER TABLE [dbo].[CONTRACT]
	ADD
	CONSTRAINT [DF__CONTRACT__Create__50C5FA01]
	DEFAULT (CONVERT([time],getdate(),(0))) FOR [Created_Time]
GO
ALTER TABLE [dbo].[CONTRACT]
	ADD
	CONSTRAINT [DF__CONTRACT__modifi__689D8392]
	DEFAULT (suser_name()) FOR [modified_by2]
GO
ALTER TABLE [dbo].[CONTRACT]
	ADD
	CONSTRAINT [DF__CONTRACT__create__6991A7CB]
	DEFAULT (suser_name()) FOR [created_by2]
GO
ALTER TABLE [dbo].[CONTRACT]
	ADD
	CONSTRAINT [DF__CONTRACT__create__6A85CC04]
	DEFAULT (getdate()) FOR [created_date2]
GO
ALTER TABLE [dbo].[CONTRACT]
	ADD
	CONSTRAINT [DF__CONTRACT__modifi__6B79F03D]
	DEFAULT (getdate()) FOR [modified_date2]
GO
CREATE NONCLUSTERED INDEX [CONTRACT$CONRACT_ID]
	ON [dbo].[CONTRACT] ([CONTRACT_ID])
	ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[CONRACT_ID]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'INDEX', N'CONTRACT$CONRACT_ID'
GO
CREATE NONCLUSTERED INDEX [CONTRACT$INSTALLATION_ID]
	ON [dbo].[CONTRACT] ([INSTALLATION_ID_link])
	ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[INSTALLATION_ID]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'INDEX', N'CONTRACT$INSTALLATION_ID'
GO
CREATE NONCLUSTERED INDEX [CONTRACT$Postal_Code]
	ON [dbo].[CONTRACT] ([NR_Postal_Code])
	ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Postal_Code]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'INDEX', N'CONTRACT$Postal_Code'
GO
CREATE NONCLUSTERED INDEX [CONTRACT$Sort_Code]
	ON [dbo].[CONTRACT] ([Sort_Code])
	ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Sort_Code]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'INDEX', N'CONTRACT$Sort_Code'
GO
CREATE NONCLUSTERED INDEX [_dta_index_CONTRACT_29_75147313__K2_K1_K36_3_9_10_21_22_37_38]
	ON [dbo].[CONTRACT] ([BP_ID_link], [CONTRACT_ID], [INSTALLATION_ID_link])
	INCLUDE ([Account_Status], [SAP_BP], [SAP_CA], [Third_Party], [Third_Party_Ref], [Move_In], [Move_Out])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_CONTRACT_29_75147313__K36_K1_K2_3_9_10_21_22_37_38_39_69_70]
	ON [dbo].[CONTRACT] ([INSTALLATION_ID_link], [CONTRACT_ID], [BP_ID_link])
	INCLUDE ([Account_Status], [SAP_BP], [SAP_CA], [Third_Party], [Third_Party_Ref], [Move_In], [Move_Out], [IMPORTANT], [VAT_Invoice_Manual], [VAT_Invoice_Self_Billing])
	ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_75147313_1_36]
	ON [dbo].[CONTRACT] ([CONTRACT_ID], [INSTALLATION_ID_link])
GO
CREATE STATISTICS [_dta_stat_75147313_36_2_1]
	ON [dbo].[CONTRACT] ([INSTALLATION_ID_link], [BP_ID_link], [CONTRACT_ID])
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[CONTRACT_ID]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'CONTRACT_ID'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[BP_ID_link]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'BP_ID_link'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Account_Status]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'Account_Status'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Payment_Method]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'Payment_Method'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Bank_Acc_Name]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'Bank_Acc_Name'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Sort_Code]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'Sort_Code'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Bank_Acc_Number]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'Bank_Acc_Number'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Bank_Reference]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'Bank_Reference'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[SAP_BP]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'SAP_BP'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[SAP_CA]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'SAP_CA'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[SAP_Contract]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'SAP_Contract'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Terms_and_Conditions_Sent_Out_Date]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'Terms_and_Conditions_Sent_Out_Date'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Terms_and_Conditions_Agreed_Date]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'Terms_and_Conditions_Agreed_Date'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Terms_and_Conditions_Agreed]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'Terms_and_Conditions_Agreed'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Proof_of_Ownership_Received]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'Proof_of_Ownership_Received'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Date_of_Last_Reaffirmation]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'Date_of_Last_Reaffirmation'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[RO_Transfer]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'RO_Transfer'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Final_bill_paid]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'Final_bill_paid'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Grant_Received]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'Grant_Received'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Grant_Form_Completed]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'Grant_Form_Completed'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Third_Party]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'Third_Party'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Third_Party_Ref]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'Third_Party_Ref'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[NR_Company]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'NR_Company'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[NR_Title]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'NR_Title'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[NR_First_Name]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'NR_First_Name'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[NR_Last_Name]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'NR_Last_Name'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[NR_Business_Phone]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'NR_Business_Phone'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[NR_Home_Phone]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'NR_Home_Phone'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[NR_Mobile_Phone]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'NR_Mobile_Phone'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[NR_Email_Address]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'NR_Email_Address'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[NR_Address_1]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'NR_Address_1'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[NR_Address_2]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'NR_Address_2'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[NR_City]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'NR_City'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[NR_County]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'NR_County'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[NR_Postal_Code]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'NR_Postal_Code'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[INSTALLATION_ID_link]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'INSTALLATION_ID_link'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Move_In]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'Move_In'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Move_Out]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'Move_Out'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[IMPORTANT]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'IMPORTANT'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Modified_By]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'Modified_By'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Date_Modified]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'Date_Modified'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[CONTRACT].[Time_Modified]', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', 'COLUMN', N'Time_Modified'
GO
GRANT ALTER
	ON [dbo].[CONTRACT]
	TO [admin_role]
GO
GRANT DELETE
	ON [dbo].[CONTRACT]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[CONTRACT]
	TO [admin_role]
GO
GRANT SELECT
	ON [dbo].[CONTRACT]
	TO [admin_role]
GO
GRANT UPDATE
	ON [dbo].[CONTRACT]
	TO [admin_role]
GO
GRANT VIEW DEFINITION
	ON [dbo].[CONTRACT]
	TO [admin_role]
GO
GRANT VIEW CHANGE TRACKING
	ON [dbo].[CONTRACT]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[CONTRACT]
	TO [userRole]
GO
GRANT SELECT
	ON [dbo].[CONTRACT]
	TO [userRole]
GO
GRANT UPDATE
	ON [dbo].[CONTRACT]
	TO [userRole]
GO
ALTER TABLE [dbo].[CONTRACT] SET (LOCK_ESCALATION = TABLE)
GO

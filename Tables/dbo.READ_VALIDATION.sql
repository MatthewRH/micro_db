SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[READ_VALIDATION] (
		[ID]                  [int] IDENTITY(1, 1) NOT NULL,
		[FiT_ID]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[New_Read_Type]       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[New_Read_Reason]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[New_Read_Date]       [datetime] NULL,
		[New_Gen_Read]        [int] NULL,
		[New_Exp_Read]        [int] NULL,
		[Fund]                [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[READ_VALIDATION]
	ADD
	CONSTRAINT [READ_VALIDATION$PrimaryKey]
	PRIMARY KEY
	CLUSTERED
	([ID])
	ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READ_VALIDATION].[PrimaryKey]', 'SCHEMA', N'dbo', 'TABLE', N'READ_VALIDATION', 'CONSTRAINT', N'READ_VALIDATION$PrimaryKey'
GO
CREATE NONCLUSTERED INDEX [READ_VALIDATION$FiT_ID]
	ON [dbo].[READ_VALIDATION] ([FiT_ID])
	ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READ_VALIDATION].[FiT_ID]', 'SCHEMA', N'dbo', 'TABLE', N'READ_VALIDATION', 'INDEX', N'READ_VALIDATION$FiT_ID'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READ_VALIDATION]', 'SCHEMA', N'dbo', 'TABLE', N'READ_VALIDATION', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READ_VALIDATION].[ID]', 'SCHEMA', N'dbo', 'TABLE', N'READ_VALIDATION', 'COLUMN', N'ID'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READ_VALIDATION].[FiT_ID]', 'SCHEMA', N'dbo', 'TABLE', N'READ_VALIDATION', 'COLUMN', N'FiT_ID'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READ_VALIDATION].[New_Read_Type]', 'SCHEMA', N'dbo', 'TABLE', N'READ_VALIDATION', 'COLUMN', N'New_Read_Type'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READ_VALIDATION].[New_Read_Reason]', 'SCHEMA', N'dbo', 'TABLE', N'READ_VALIDATION', 'COLUMN', N'New_Read_Reason'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READ_VALIDATION].[New_Read_Date]', 'SCHEMA', N'dbo', 'TABLE', N'READ_VALIDATION', 'COLUMN', N'New_Read_Date'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READ_VALIDATION].[New_Gen_Read]', 'SCHEMA', N'dbo', 'TABLE', N'READ_VALIDATION', 'COLUMN', N'New_Gen_Read'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[READ_VALIDATION].[New_Exp_Read]', 'SCHEMA', N'dbo', 'TABLE', N'READ_VALIDATION', 'COLUMN', N'New_Exp_Read'
GO
GRANT ALTER
	ON [dbo].[READ_VALIDATION]
	TO [admin_role]
GO
GRANT DELETE
	ON [dbo].[READ_VALIDATION]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[READ_VALIDATION]
	TO [admin_role]
GO
GRANT SELECT
	ON [dbo].[READ_VALIDATION]
	TO [admin_role]
GO
GRANT UPDATE
	ON [dbo].[READ_VALIDATION]
	TO [admin_role]
GO
GRANT VIEW DEFINITION
	ON [dbo].[READ_VALIDATION]
	TO [admin_role]
GO
GRANT VIEW CHANGE TRACKING
	ON [dbo].[READ_VALIDATION]
	TO [admin_role]
GO
GRANT DELETE
	ON [dbo].[READ_VALIDATION]
	TO [userRole]
GO
GRANT INSERT
	ON [dbo].[READ_VALIDATION]
	TO [userRole]
GO
GRANT SELECT
	ON [dbo].[READ_VALIDATION]
	TO [userRole]
GO
GRANT UPDATE
	ON [dbo].[READ_VALIDATION]
	TO [userRole]
GO
ALTER TABLE [dbo].[READ_VALIDATION] SET (LOCK_ESCALATION = TABLE)
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Failed_Visits] (
		[Installation_ID]     [int] NOT NULL,
		[Read_ID]             [int] IDENTITY(1, 1) NOT NULL,
		[Visit_Date]          [datetime] NULL,
		[MSN]                 [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Request_Status]      [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Visit_Number]        [int] NULL,
		[Notes_A]             [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Notes_B]             [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Created_By]          [varchar](255) COLLATE Latin1_General_CI_AS NOT NULL,
		[Created_Date]        [datetime] NOT NULL,
		[Modified_By]         [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Modified_Date]       [datetime] NOT NULL,
		[Status]              [varchar](255) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Failed_Visits]
	ADD
	CONSTRAINT [Failed_visits$PrimaryKey]
	PRIMARY KEY
	CLUSTERED
	([Read_ID])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Failed_Visits]
	ADD
	CONSTRAINT [DF__Failed_Vi__Creat__5E1FF51F]
	DEFAULT (suser_name()) FOR [Created_By]
GO
ALTER TABLE [dbo].[Failed_Visits]
	ADD
	CONSTRAINT [DF__Failed_Vi__Creat__5F141958]
	DEFAULT (getdate()) FOR [Created_Date]
GO
ALTER TABLE [dbo].[Failed_Visits]
	ADD
	CONSTRAINT [DF__Failed_Vi__Modif__60083D91]
	DEFAULT (suser_name()) FOR [Modified_By]
GO
ALTER TABLE [dbo].[Failed_Visits]
	ADD
	CONSTRAINT [DF__Failed_Vi__Modif__60FC61CA]
	DEFAULT (getdate()) FOR [Modified_Date]
GO
GRANT ALTER
	ON [dbo].[Failed_Visits]
	TO [admin_role]
GO
GRANT DELETE
	ON [dbo].[Failed_Visits]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[Failed_Visits]
	TO [admin_role]
GO
GRANT SELECT
	ON [dbo].[Failed_Visits]
	TO [admin_role]
GO
GRANT UPDATE
	ON [dbo].[Failed_Visits]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[Failed_Visits]
	TO [userRole]
GO
GRANT SELECT
	ON [dbo].[Failed_Visits]
	TO [userRole]
GO
GRANT UPDATE
	ON [dbo].[Failed_Visits]
	TO [userRole]
GO
ALTER TABLE [dbo].[Failed_Visits] SET (LOCK_ESCALATION = TABLE)
GO

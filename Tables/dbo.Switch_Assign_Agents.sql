SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Switch_Assign_Agents] (
		[FIT_ID]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Assigned_to]     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Move_In]         [datetime] NULL
) ON [PRIMARY]
GO
GRANT ALTER
	ON [dbo].[Switch_Assign_Agents]
	TO [admin_role]
GO
GRANT DELETE
	ON [dbo].[Switch_Assign_Agents]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[Switch_Assign_Agents]
	TO [admin_role]
GO
GRANT SELECT
	ON [dbo].[Switch_Assign_Agents]
	TO [admin_role]
GO
GRANT UPDATE
	ON [dbo].[Switch_Assign_Agents]
	TO [admin_role]
GO
GRANT VIEW DEFINITION
	ON [dbo].[Switch_Assign_Agents]
	TO [admin_role]
GO
GRANT VIEW CHANGE TRACKING
	ON [dbo].[Switch_Assign_Agents]
	TO [admin_role]
GO
GRANT DELETE
	ON [dbo].[Switch_Assign_Agents]
	TO [userRole]
GO
GRANT INSERT
	ON [dbo].[Switch_Assign_Agents]
	TO [userRole]
GO
GRANT SELECT
	ON [dbo].[Switch_Assign_Agents]
	TO [userRole]
GO
GRANT UPDATE
	ON [dbo].[Switch_Assign_Agents]
	TO [userRole]
GO
ALTER TABLE [dbo].[Switch_Assign_Agents] SET (LOCK_ESCALATION = TABLE)
GO

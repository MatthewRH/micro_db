SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bulk_COT] (
		[FIT_ID]               [nvarchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
		[Third_Party_Ref]      [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Address_1]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Address_2]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Postcode]             [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Previous_Reading]     [int] NULL,
		[New_Reading]          [int] NULL,
		[New_Reading_Date]     [datetime] NULL,
		[Old_3rd_Party]        [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[New_3rd_Party]        [nvarchar](50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
GRANT DELETE
	ON [dbo].[Bulk_COT]
	TO [userRole]
GO
GRANT INSERT
	ON [dbo].[Bulk_COT]
	TO [userRole]
GO
GRANT SELECT
	ON [dbo].[Bulk_COT]
	TO [userRole]
GO
GRANT UPDATE
	ON [dbo].[Bulk_COT]
	TO [userRole]
GO
ALTER TABLE [dbo].[Bulk_COT] SET (LOCK_ESCALATION = TABLE)
GO

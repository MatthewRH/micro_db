SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ESP_Annual] (
		[Contract_Number]                  [nvarchar](30) COLLATE Latin1_General_CI_AS NULL,
		[Net_Suite_Site_ID]                [int] NULL,
		[Year]                             [int] NULL,
		[PPA_Payment]                      [float] NULL,
		[PPA_Anticipated_Output]           [float] NULL,
		[PPA_Guaranteed_Annual_Output]     [float] NULL,
		[installation_id_link]             [int] NOT NULL,
		[ESP_ID]                           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
GRANT DELETE
	ON [dbo].[ESP_Annual]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[ESP_Annual]
	TO [admin_role]
GO
GRANT SELECT
	ON [dbo].[ESP_Annual]
	TO [admin_role]
GO
GRANT UPDATE
	ON [dbo].[ESP_Annual]
	TO [admin_role]
GO
GRANT SELECT
	ON [dbo].[ESP_Annual]
	TO [userRole]
GO
ALTER TABLE [dbo].[ESP_Annual] SET (LOCK_ESCALATION = TABLE)
GO

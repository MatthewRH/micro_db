SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ESP_Contract_New] (
		[ESP_Contract_ID]                [int] IDENTITY(1, 1) NOT NULL,
		[Contract_ID_Link]               [int] NOT NULL,
		[SAP_BP]                         [int] NULL,
		[SAP_CA]                         [int] NULL,
		[ESP_ID]                         [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Account_Status]                 [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Invoice_Route]                  [nvarchar](20) COLLATE Latin1_General_CI_AS NULL,
		[First_Name]                     [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Last_Name]                      [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Address]                        [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Address_2]                      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Address_3]                      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[City]                           [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[County]                         [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[PostCode]                       [nvarchar](12) COLLATE Latin1_General_CI_AS NULL,
		[Country]                        [nvarchar](20) COLLATE Latin1_General_CI_AS NULL,
		[Phone_Number]                   [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Mobile_Number]                  [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Email_Address]                  [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Drivers_Licence_Number]         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[National_Insurance_Number]      [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Date_Of_Birth]                  [datetime2](7) NULL,
		[Title_Number]                   [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Sun_Edison_Contract_Number]     [nvarchar](30) COLLATE Latin1_General_CI_AS NULL,
		[Bank_Account]                   [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Sort_Code]                      [nvarchar](20) COLLATE Latin1_General_CI_AS NULL,
		[BIC_Swift_Code]                 [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[First_Payment_Date]             [datetime2](7) NULL,
		[Annual_Escalation]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Product_Type]                   [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PeGu_Start_Date]                [datetime2](7) NULL,
		[PeGu]                           [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[PeGu_Percentage]                [nvarchar](10) COLLATE Latin1_General_CI_AS NULL,
		[Promotion_Code]                 [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Tax_Depreciation_Basis]         [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[VAT_Applicable]                 [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Payment_Frequency]              [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Payment_Option]                 [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Bank_Acc_Name]                  [nvarchar](150) COLLATE Latin1_General_CI_AS NULL,
		[Payment_Method]                 [nvarchar](5) COLLATE Latin1_General_CI_AS NULL,
		[Move_In_ESP]                    [datetime2](7) NULL,
		[Move_Out_ESP]                   [datetime2](7) NULL,
		[Initial_System_Tariff_Rate]     [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Initial_System_Export_Rate]     [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Account_Holder_Name]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ESP_Contract_New]
	ADD
	CONSTRAINT [pk_ESP_Contract3]
	PRIMARY KEY
	CLUSTERED
	([ESP_Contract_ID])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[ESP_Contract_New] SET (LOCK_ESCALATION = TABLE)
GO

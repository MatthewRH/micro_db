SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ASG_Switch] (
		[FIT_ID]                         [nvarchar](13) COLLATE Latin1_General_CI_AS NULL,
		[Assigned_Agent]                 [nvarchar](8) COLLATE Latin1_General_CI_AS NULL,
		[Raised_By]                      [nvarchar](8) COLLATE Latin1_General_CI_AS NULL,
		[Raised_Date]                    [datetime] NULL,
		[Switch_Attempt]                 [int] NULL,
		[Proposed_Switch_Date]           [datetime] NULL,
		[Completed_By]                   [nvarchar](8) COLLATE Latin1_General_CI_AS NULL,
		[Completed_Date]                 [datetime] NULL,
		[Error_1]                        [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Error_1_Responsible_Party]      [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Error_1_Resolved_By]            [nvarchar](8) COLLATE Latin1_General_CI_AS NULL,
		[Error_2]                        [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Error_2_Responsible_Party]      [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Error_2_Resolved_By]            [nvarchar](8) COLLATE Latin1_General_CI_AS NULL,
		[Error_3]                        [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Error_3_Responsible_Party]      [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Error_3_Resolved_By]            [nvarchar](8) COLLATE Latin1_General_CI_AS NULL,
		[Expiry_Reason_1]                [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Expiry_Reason_2]                [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Expiry_Reason_3]                [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Expiry_Reason_4]                [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Expiry_Reason_5]                [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Expiry_date]                    AS (dateadd(day,(28),[Proposed_Switch_Date])),
		[Switch_Accepted_Date]           [datetime] NULL,
		[Error1_Recieved_Date]           [datetime] NULL,
		[Error2_Recieved_Date]           [datetime] NULL,
		[Error3_Recieved_Date]           [datetime] NULL,
		[Assigned_Agent_To_Complete]     [nvarchar](8) COLLATE Latin1_General_CI_AS NULL,
		[Target_Date]                    [datetime] NULL,
		[Switch_ID]                      [int] IDENTITY(1, 1) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ASG_Switch]
	ADD
	CONSTRAINT [PK_ASG_Switch]
	PRIMARY KEY
	NONCLUSTERED
	([Switch_ID])
	ON [PRIMARY]
GO
GRANT ALTER
	ON [dbo].[ASG_Switch]
	TO [admin_role]
GO
GRANT DELETE
	ON [dbo].[ASG_Switch]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[ASG_Switch]
	TO [admin_role]
GO
GRANT SELECT
	ON [dbo].[ASG_Switch]
	TO [admin_role]
GO
GRANT UPDATE
	ON [dbo].[ASG_Switch]
	TO [admin_role]
GO
GRANT VIEW DEFINITION
	ON [dbo].[ASG_Switch]
	TO [admin_role]
GO
GRANT VIEW CHANGE TRACKING
	ON [dbo].[ASG_Switch]
	TO [admin_role]
GO
GRANT DELETE
	ON [dbo].[ASG_Switch]
	TO [userRole]
GO
GRANT INSERT
	ON [dbo].[ASG_Switch]
	TO [userRole]
GO
GRANT SELECT
	ON [dbo].[ASG_Switch]
	TO [userRole]
GO
GRANT UPDATE
	ON [dbo].[ASG_Switch]
	TO [userRole]
GO
ALTER TABLE [dbo].[ASG_Switch] SET (LOCK_ESCALATION = TABLE)
GO

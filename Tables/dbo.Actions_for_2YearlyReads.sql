SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Actions_for_2YearlyReads] (
		[record_id]             [int] IDENTITY(1, 1) NOT NULL,
		[Installation_ID]       [int] NOT NULL,
		[Date_Note_Made]        [datetime] NULL,
		[Action]                [varchar](max) COLLATE Latin1_General_CI_AS NULL,
		[Created_By]            [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Status]                [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Date_Visit_Booked]     [datetime] NULL,
		[Modified_date]         [datetime] NULL,
		[Modified_by]           [varchar](255) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Actions_for_2YearlyReads]
	ADD
	CONSTRAINT [Actions_for_2YearlyReads_pk]
	PRIMARY KEY
	CLUSTERED
	([record_id])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Actions_for_2YearlyReads]
	ADD
	CONSTRAINT [DF__Actions_f__Date___08162EEB]
	DEFAULT (getdate()) FOR [Date_Note_Made]
GO
ALTER TABLE [dbo].[Actions_for_2YearlyReads]
	ADD
	CONSTRAINT [DF__Actions_f__Creat__090A5324]
	DEFAULT (suser_name()) FOR [Created_By]
GO
ALTER TABLE [dbo].[Actions_for_2YearlyReads]
	ADD
	CONSTRAINT [DF__Actions_f__Modif__09FE775D]
	DEFAULT (getdate()) FOR [Modified_date]
GO
ALTER TABLE [dbo].[Actions_for_2YearlyReads]
	ADD
	CONSTRAINT [DF__Actions_f__Modif__0AF29B96]
	DEFAULT (suser_name()) FOR [Modified_by]
GO
GRANT ALTER
	ON [dbo].[Actions_for_2YearlyReads]
	TO [admin_role]
GO
GRANT DELETE
	ON [dbo].[Actions_for_2YearlyReads]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[Actions_for_2YearlyReads]
	TO [admin_role]
GO
GRANT SELECT
	ON [dbo].[Actions_for_2YearlyReads]
	TO [admin_role]
GO
GRANT UPDATE
	ON [dbo].[Actions_for_2YearlyReads]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[Actions_for_2YearlyReads]
	TO [userRole]
GO
GRANT SELECT
	ON [dbo].[Actions_for_2YearlyReads]
	TO [userRole]
GO
GRANT UPDATE
	ON [dbo].[Actions_for_2YearlyReads]
	TO [userRole]
GO
ALTER TABLE [dbo].[Actions_for_2YearlyReads] SET (LOCK_ESCALATION = TABLE)
GO

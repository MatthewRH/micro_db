SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BP_ARCHIVE] (
		[BP_ID]                     [int] NOT NULL,
		[GeneratorID]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Customer_Type]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Company]                   [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Title]                     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Last_Name]                 [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[First_Name]                [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[VAT_Reg_Number]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Email_Address]             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Business_Phone]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Home_Phone]                [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Mobile_Phone]              [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Fax_Number]                [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Paperless_Customer]        [bit] NULL,
		[Contact_Method_Email]      [bit] NULL,
		[Contact_Method_Letter]     [bit] NULL,
		[Address_1]                 [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Address_2]                 [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[City]                      [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[County]                    [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Postal_Code]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Added_To_SAP]              [bit] NULL,
		[ID_Received]               [bit] NULL,
		[Modified_By]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Date_Modified]             [datetime] NULL,
		[Time_Modified]             [datetime] NULL,
		[Created_By]                [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Created_Date]              [date] NULL,
		[Created_Time]              [time](7) NULL,
		[Modified_By2]              [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Created_By2]               [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Created_Date2]             [datetime] NULL,
		[Modified_Date2]            [datetime] NULL,
		[Email_2]                   [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Email_3]                   [nvarchar](255) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BP_ARCHIVE]
	ADD
	CONSTRAINT [BP_Archive$PrimaryKey]
	PRIMARY KEY
	CLUSTERED
	([BP_ID])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[BP_ARCHIVE]
	ADD
	CONSTRAINT [DF__BP_ARCHIV__Paper__546180BB]
	DEFAULT ((0)) FOR [Paperless_Customer]
GO
ALTER TABLE [dbo].[BP_ARCHIVE]
	ADD
	CONSTRAINT [DF__BP_ARCHIV__Conta__5555A4F4]
	DEFAULT ((0)) FOR [Contact_Method_Email]
GO
ALTER TABLE [dbo].[BP_ARCHIVE]
	ADD
	CONSTRAINT [DF__BP_ARCHIV__Conta__5649C92D]
	DEFAULT ((0)) FOR [Contact_Method_Letter]
GO
ALTER TABLE [dbo].[BP_ARCHIVE]
	ADD
	CONSTRAINT [DF__BP_ARCHIV__Added__573DED66]
	DEFAULT ((0)) FOR [Added_To_SAP]
GO
ALTER TABLE [dbo].[BP_ARCHIVE]
	ADD
	CONSTRAINT [DF__BP_ARCHIV__ID_Re__5832119F]
	DEFAULT ((0)) FOR [ID_Received]
GO
ALTER TABLE [dbo].[BP_ARCHIVE]
	ADD
	CONSTRAINT [DF__BP_ARCHIV__Creat__592635D8]
	DEFAULT (getdate()) FOR [Created_Date]
GO
ALTER TABLE [dbo].[BP_ARCHIVE]
	ADD
	CONSTRAINT [DF__BP_ARCHIV__Creat__5A1A5A11]
	DEFAULT (CONVERT([time],getdate(),(0))) FOR [Created_Time]
GO
ALTER TABLE [dbo].[BP_ARCHIVE]
	ADD
	CONSTRAINT [DF__BP_ARCHIV__Modif__5B0E7E4A]
	DEFAULT (suser_name()) FOR [Modified_By2]
GO
ALTER TABLE [dbo].[BP_ARCHIVE]
	ADD
	CONSTRAINT [DF__BP_ARCHIV__Creat__5C02A283]
	DEFAULT (suser_name()) FOR [Created_By2]
GO
ALTER TABLE [dbo].[BP_ARCHIVE]
	ADD
	CONSTRAINT [DF__BP_ARCHIV__Creat__5CF6C6BC]
	DEFAULT (getdate()) FOR [Created_Date2]
GO
ALTER TABLE [dbo].[BP_ARCHIVE]
	ADD
	CONSTRAINT [DF__BP_ARCHIV__Modif__5DEAEAF5]
	DEFAULT (getdate()) FOR [Modified_Date2]
GO
ALTER TABLE [dbo].[BP_ARCHIVE] SET (LOCK_ESCALATION = TABLE)
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[INSTALLATION] (
		[INSTALLATION_ID]                       [int] IDENTITY(1, 1) NOT NULL,
		[Scheme_Type]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Supply_MPAN]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Export_MPAN]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Export_Status]                         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Address_1]                             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Address_2]                             [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[City]                                  [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[County]                                [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Postal_Code]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[OS_Grid_Ref]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Type_of_Installation]                  [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Generation_Type]                       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Generation_Capacity]                   [int] NULL,
		[Generation_MSN]                        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Export_MSN]                            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Total_Installed_Capacity_kW]           [float] NULL,
		[Total_Declared_Net_Capacity_kW]        [float] NULL,
		[Estimated_Annual_Generation]           [int] NULL,
		[RO_Accreditation_Number]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Make_of_Generation_Equipment]          [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Model_of_Generation_Equipment]         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Installation_Name]                     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Generation_Meter_Make_and_Model]       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Agent_Appointment_Letter_Received]     [bit] NULL,
		[LOA_Received]                          [bit] NULL,
		[Electrical_Schematic_Received]         [bit] NULL,
		[EPC_Reference]                         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Application_Received_Date]             [datetime] NULL,
		[FiT_ID_Initial]                        [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[MCS_Certification_Number_1]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Initial_System_Tariff_Rate]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Initial_System_Export_Rate]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Initial_System_Pct_Split]              [float] NULL,
		[Initial_System_Start_Date]             [datetime] NULL,
		[Initial_System_End_Date]               [datetime] NULL,
		[Initial_System_Commissioning_Date]     [datetime] NULL,
		[FiT_ID_Ext1]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[MCS_Certification_Number_2]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Extension_1_Tariff_Rate]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Extension_1_Export_Rate]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Extension_1_Pct_Split]                 [float] NULL,
		[Extension_1_Start_Date]                [datetime] NULL,
		[Extension_1_End_Date]                  [datetime] NULL,
		[Extension_1_Commissioning_Date]        [datetime] NULL,
		[FiT_ID_Ext2]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[MCS_Certification_Number_3]            [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Extension_2_Tariff_Rate]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Extension_2_Export_Rate]               [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Extension_2_Pct_Split]                 [float] NULL,
		[Extension_2_Start_Date]                [datetime] NULL,
		[Extension_2_End_Date]                  [datetime] NULL,
		[Extension_2_Commissioning_Date]        [datetime] NULL,
		[TopUp_Tariff_Rate]                     [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Modified_By]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Date_Modified]                         [datetime] NULL,
		[Time_Modified]                         [datetime] NULL,
		[Cos_Flag]                              [bit] NULL,
		[SysDesc]                               [nvarchar](20) COLLATE Latin1_General_CI_AS NULL,
		[ExpPct1]                               [float] NULL,
		[ExpPct2]                               [float] NULL,
		[InitSysExpRate2]                       [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[FiT_Related]                           [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[DC]                                    [nvarchar](4) COLLATE Latin1_General_CI_AS NULL,
		[Created_By]                            [varchar](100) COLLATE Latin1_General_CI_AS NULL,
		[Created_Date]                          [date] NULL,
		[Created_Time]                          [time](7) NULL,
		[Export_Meter_Photo]                    [bit] NULL,
		[Confirmation_Date]                     [datetime] NULL,
		[Ext1_Confirmation_Date]                [datetime] NULL,
		[Ext2_Confirmation_Date]                [datetime] NULL,
		[Modified_by2]                          [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Created_by2]                           [varchar](255) COLLATE Latin1_General_CI_AS NULL,
		[Created_date2]                         [datetime] NULL,
		[Modified_date2]                        [datetime] NULL,
		[Remotely_Read_Enabled_From]            [datetime] NULL,
		[Remotely_Read_Enabled]                 [bit] NULL,
		[Epc_Date]                              [datetime] NULL,
		[Generation_Meter_Model]                [nvarchar](100) COLLATE Latin1_General_CI_AS NULL,
		[SSMA_TimeStamp]                        [timestamp] NOT NULL,
		[Valid_From]                            [datetime] NULL,
		[Initial_Meter_Reading]                 [decimal](8, 2) NULL,
		[Initial_Meter_Read_Date]               [datetime] NULL,
		[Originating_Client_Reference]          [varchar](45) COLLATE Latin1_General_CI_AS NULL,
		[Date_Last_Contact]                     [date] NULL,
		[MCS_Issue_Date]                        [datetime] NULL,
		[MCS_Issue_Time]                        [datetime] NULL,
		[COT_Flag]                              [bit] NULL,
		[ESP_ID]                                [nvarchar](50) COLLATE Latin1_General_CI_AS NULL,
		[Partners_Name]                         [nvarchar](255) COLLATE Latin1_General_CI_AS NULL,
		[FMV]                                   [money] NULL,
		[Equipment_Cost]                        [money] NULL,
		[Register_Digits]                       [numeric](2, 0) NULL,
		[Switch_ID]                             [varchar](50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[INSTALLATION]
	ADD
	CONSTRAINT [INSTALLATION$PrimaryKey]
	PRIMARY KEY
	CLUSTERED
	([INSTALLATION_ID])
	ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[PrimaryKey]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'CONSTRAINT', N'INSTALLATION$PrimaryKey'
GO
ALTER TABLE [dbo].[INSTALLATION]
	ADD
	CONSTRAINT [DF__INSTALLAT__Schem__0C1BC9F9]
	DEFAULT ('FiT') FOR [Scheme_Type]
GO
ALTER TABLE [dbo].[INSTALLATION]
	ADD
	CONSTRAINT [DF__INSTALLAT__Agent__0D0FEE32]
	DEFAULT ((0)) FOR [Agent_Appointment_Letter_Received]
GO
ALTER TABLE [dbo].[INSTALLATION]
	ADD
	CONSTRAINT [DF__INSTALLAT__LOA_R__0E04126B]
	DEFAULT ((0)) FOR [LOA_Received]
GO
ALTER TABLE [dbo].[INSTALLATION]
	ADD
	CONSTRAINT [DF__INSTALLAT__Elect__0EF836A4]
	DEFAULT ((0)) FOR [Electrical_Schematic_Received]
GO
ALTER TABLE [dbo].[INSTALLATION]
	ADD
	CONSTRAINT [DF__INSTALLAT__Initi__0FEC5ADD]
	DEFAULT ((1)) FOR [Initial_System_Pct_Split]
GO
ALTER TABLE [dbo].[INSTALLATION]
	ADD
	CONSTRAINT [DF__INSTALLAT__Exten__10E07F16]
	DEFAULT ((0)) FOR [Extension_1_Pct_Split]
GO
ALTER TABLE [dbo].[INSTALLATION]
	ADD
	CONSTRAINT [DF__INSTALLAT__Exten__11D4A34F]
	DEFAULT ((0)) FOR [Extension_2_Pct_Split]
GO
ALTER TABLE [dbo].[INSTALLATION]
	ADD
	CONSTRAINT [DF__INSTALLAT__Creat__4C0144E4]
	DEFAULT (getdate()) FOR [Created_Date]
GO
ALTER TABLE [dbo].[INSTALLATION]
	ADD
	CONSTRAINT [DF__INSTALLAT__Creat__4CF5691D]
	DEFAULT (CONVERT([time],getdate(),(0))) FOR [Created_Time]
GO
ALTER TABLE [dbo].[INSTALLATION]
	ADD
	CONSTRAINT [DF__INSTALLAT__Modif__6D6238AF]
	DEFAULT (suser_name()) FOR [Modified_by2]
GO
ALTER TABLE [dbo].[INSTALLATION]
	ADD
	CONSTRAINT [DF__INSTALLAT__Creat__6E565CE8]
	DEFAULT (suser_name()) FOR [Created_by2]
GO
ALTER TABLE [dbo].[INSTALLATION]
	ADD
	CONSTRAINT [DF__INSTALLAT__Creat__6F4A8121]
	DEFAULT (getdate()) FOR [Created_date2]
GO
ALTER TABLE [dbo].[INSTALLATION]
	ADD
	CONSTRAINT [DF__INSTALLAT__Modif__7132C993]
	DEFAULT (getdate()) FOR [Modified_date2]
GO
CREATE NONCLUSTERED INDEX [INSTALLATION$FiT_ID]
	ON [dbo].[INSTALLATION] ([FiT_ID_Initial])
	ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[FiT_ID]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'INDEX', N'INSTALLATION$FiT_ID'
GO
CREATE NONCLUSTERED INDEX [INSTALLATION$INSTALLATION_ID]
	ON [dbo].[INSTALLATION] ([INSTALLATION_ID])
	ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[INSTALLATION_ID]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'INDEX', N'INSTALLATION$INSTALLATION_ID'
GO
CREATE NONCLUSTERED INDEX [INSTALLATION$Postal_Code]
	ON [dbo].[INSTALLATION] ([Postal_Code])
	ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Postal_Code]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'INDEX', N'INSTALLATION$Postal_Code'
GO
CREATE NONCLUSTERED INDEX [_dta_index_INSTALLATION_29_187147712__K1_3_6_7_8_9_10_13_15_30]
	ON [dbo].[INSTALLATION] ([INSTALLATION_ID])
	INCLUDE ([Supply_MPAN], [Address_1], [Address_2], [City], [County], [Postal_Code], [Generation_Type], [Generation_MSN], [FiT_ID_Initial])
	ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[INSTALLATION_ID]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'INSTALLATION_ID'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Scheme_Type]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Scheme_Type'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Supply_MPAN]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Supply_MPAN'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Export_MPAN]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Export_MPAN'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Export_Status]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Export_Status'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Address_1]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Address_1'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Address_2]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Address_2'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[City]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'City'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[County]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'County'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Postal_Code]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Postal_Code'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[OS_Grid_Ref]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'OS_Grid_Ref'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Type_of_Installation]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Type_of_Installation'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Generation_Type]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Generation_Type'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Generation_Capacity]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Generation_Capacity'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Generation_MSN]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Generation_MSN'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Export_MSN]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Export_MSN'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Total_Installed_Capacity_kW]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Total_Installed_Capacity_kW'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Total_Declared_Net_Capacity_kW]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Total_Declared_Net_Capacity_kW'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Estimated_Annual_Generation]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Estimated_Annual_Generation'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[RO_Accreditation_Number]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'RO_Accreditation_Number'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Make_of_Generation_Equipment]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Make_of_Generation_Equipment'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Model_of_Generation_Equipment]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Model_of_Generation_Equipment'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Installation_Name]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Installation_Name'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Generation_Meter_Make_and_Model]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Generation_Meter_Make_and_Model'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Agent_Appointment_Letter_Received]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Agent_Appointment_Letter_Received'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[LOA_Received]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'LOA_Received'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Electrical_Schematic_Received]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Electrical_Schematic_Received'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[EPC_Reference]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'EPC_Reference'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Application_Received_Date]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Application_Received_Date'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[FiT_ID_Initial]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'FiT_ID_Initial'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[MCS_Certification_Number_1]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'MCS_Certification_Number_1'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Initial_System_Tariff_Rate]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Initial_System_Tariff_Rate'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Initial_System_Export_Rate]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Initial_System_Export_Rate'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Initial_System_Pct_Split]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Initial_System_Pct_Split'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Initial_System_Start_Date]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Initial_System_Start_Date'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Initial_System_End_Date]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Initial_System_End_Date'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Initial_System_Commissioning_Date]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Initial_System_Commissioning_Date'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[FiT_ID_Ext1]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'FiT_ID_Ext1'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[MCS_Certification_Number_2]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'MCS_Certification_Number_2'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Extension_1_Tariff_Rate]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Extension_1_Tariff_Rate'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Extension_1_Export_Rate]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Extension_1_Export_Rate'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Extension_1_Pct_Split]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Extension_1_Pct_Split'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Extension_1_Start_Date]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Extension_1_Start_Date'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Extension_1_End_Date]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Extension_1_End_Date'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Extension_1_Commissioning_Date]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Extension_1_Commissioning_Date'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[FiT_ID_Ext2]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'FiT_ID_Ext2'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[MCS_Certification_Number_3]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'MCS_Certification_Number_3'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Extension_2_Tariff_Rate]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Extension_2_Tariff_Rate'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Extension_2_Export_Rate]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Extension_2_Export_Rate'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Extension_2_Pct_Split]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Extension_2_Pct_Split'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Extension_2_Start_Date]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Extension_2_Start_Date'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Extension_2_End_Date]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Extension_2_End_Date'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Extension_2_Commissioning_Date]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Extension_2_Commissioning_Date'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[TopUp_Tariff_Rate]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'TopUp_Tariff_Rate'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Modified_By]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Modified_By'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Date_Modified]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Date_Modified'
GO
EXEC sp_addextendedproperty N'MS_SSMA_SOURCE', N'Microtricity Customer Management System2.[INSTALLATION].[Time_Modified]', 'SCHEMA', N'dbo', 'TABLE', N'INSTALLATION', 'COLUMN', N'Time_Modified'
GO
GRANT ALTER
	ON [dbo].[INSTALLATION]
	TO [admin_role]
GO
GRANT DELETE
	ON [dbo].[INSTALLATION]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[INSTALLATION]
	TO [admin_role]
GO
GRANT SELECT
	ON [dbo].[INSTALLATION]
	TO [admin_role]
GO
GRANT UPDATE
	ON [dbo].[INSTALLATION]
	TO [admin_role]
GO
GRANT VIEW DEFINITION
	ON [dbo].[INSTALLATION]
	TO [admin_role]
GO
GRANT VIEW CHANGE TRACKING
	ON [dbo].[INSTALLATION]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[INSTALLATION]
	TO [userRole]
GO
GRANT SELECT
	ON [dbo].[INSTALLATION]
	TO [userRole]
GO
GRANT UPDATE
	ON [dbo].[INSTALLATION]
	TO [userRole]
GO
ALTER TABLE [dbo].[INSTALLATION] SET (LOCK_ESCALATION = TABLE)
GO

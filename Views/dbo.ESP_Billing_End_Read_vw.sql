SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW dbo.ESP_Billing_End_Read_vw
AS
WITH LatestRead AS (SELECT        E.ESP_Contract_ID, R.MSN, MAX(R.Meter_Read_Date) AS LatestReadDate
                                               FROM            dbo.READINGS AS R INNER JOIN
                                                                         dbo.CONTRACT AS C ON C.INSTALLATION_ID_link = R.INSTALLATION_ID_link INNER JOIN
                                                                         dbo.ESP_Contract AS E ON E.Contract_ID_Link = C.CONTRACT_ID
                                               GROUP BY E.ESP_Contract_ID, R.MSN)
    SELECT        dbo.READINGS.Meter_Read_Reason, dbo.READINGS.INSTALLATION_ID_link, dbo.READINGS.Meter_Read_Date AS Billing_End, dbo.READINGS.Generation_Read AS End_Read, 
                              dbo.READINGS.MSN AS End_MSN, dbo.READINGS.READ_ID AS END_Read_ID
     FROM            dbo.CONTRACT INNER JOIN
                              dbo.INSTALLATION ON dbo.CONTRACT.INSTALLATION_ID_link = dbo.INSTALLATION.INSTALLATION_ID LEFT OUTER JOIN
                              dbo.ESP_Contract ON dbo.CONTRACT.CONTRACT_ID = dbo.ESP_Contract.Contract_ID_Link RIGHT OUTER JOIN
                              dbo.READINGS ON dbo.INSTALLATION.INSTALLATION_ID = dbo.READINGS.INSTALLATION_ID_link INNER JOIN
                              LatestRead AS LatestRead_1 ON LatestRead_1.ESP_Contract_ID = dbo.ESP_Contract.ESP_Contract_ID AND LatestRead_1.MSN = dbo.READINGS.MSN AND 
                              LatestRead_1.LatestReadDate = dbo.READINGS.Meter_Read_Date
     WHERE        (dbo.READINGS.Meter_Read_Date BETWEEN dbo.ESP_Contract.Move_In_ESP AND dbo.ESP_Contract.Move_Out_ESP) AND (dbo.READINGS.Meter_Read_Reason NOT IN ('2 Year Audit', 'Initial', 'Initial (COT)', 
                              'Initial (MX)', 'Interim')) AND (dbo.READINGS.ESP_Billed IS NULL)
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CONTRACT"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 330
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "INSTALLATION"
            Begin Extent = 
               Top = 6
               Left = 368
               Bottom = 136
               Right = 652
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ESP_Contract"
            Begin Extent = 
               Top = 6
               Left = 690
               Bottom = 136
               Right = 939
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "READINGS"
            Begin Extent = 
               Top = 6
               Left = 977
               Bottom = 136
               Right = 1187
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LatestRead_1"
            Begin Extent = 
               Top = 6
               Left = 1225
               Bottom = 119
               Right = 1400
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
  ', 'SCHEMA', N'dbo', 'VIEW', N'ESP_Billing_End_Read_vw', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'       Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'ESP_Billing_End_Read_vw', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 2, 'SCHEMA', N'dbo', 'VIEW', N'ESP_Billing_End_Read_vw', NULL, NULL
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW dbo.Read_For_Quarter_vw
AS
SELECT        dbo.CONTRACT.CONTRACT_ID, dbo.READINGS.READ_ID, dbo.READINGS.INSTALLATION_ID_link, dbo.READINGS.Meter_Read_Type, dbo.READINGS.Meter_Read_Date AS [Meter_Read_Date(NEW)], 
                         dbo.READINGS.Meter_Read_Reason, dbo.READINGS.Generation_Read AS [Generation_Read(NEW)], dbo.READINGS.Export_Read AS [Export_Read(NEW)], dbo.READINGS.IncludedInLevelisation, 
                         dbo.READINGS.Gen_Validation, dbo.READINGS.Modified_By, dbo.READINGS.Date_Modified, dbo.READINGS.Time_Modified, dbo.READINGS.SSMA_TimeStamp, dbo.CONTRACT.Move_In, 
                         dbo.CONTRACT.Move_Out
FROM            dbo.READINGS INNER JOIN
                         dbo.CONTRACT ON dbo.READINGS.INSTALLATION_ID_link = dbo.CONTRACT.INSTALLATION_ID_link LEFT OUTER JOIN
                         dbo.Latest_Q_Read_vw ON dbo.READINGS.Meter_Read_Reason = dbo.Latest_Q_Read_vw.Latest_Quarter_Read
WHERE        (dbo.READINGS.Meter_Read_Date BETWEEN dbo.CONTRACT.Move_In AND dbo.CONTRACT.Move_Out) AND (dbo.READINGS.Meter_Read_Reason IN (dbo.Latest_Q_Read_vw.Latest_Quarter_Read, 'Final', 
                         'Final (COT)', 'Change of Supply', 'Final (COS)', 'Final (MX)')) AND (dbo.READINGS.IncludedInLevelisation = 'False')
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "READINGS"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 248
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CONTRACT"
            Begin Extent = 
               Top = 6
               Left = 286
               Bottom = 136
               Right = 578
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Latest_Q_Read_vw"
            Begin Extent = 
               Top = 6
               Left = 616
               Bottom = 85
               Right = 812
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'Read_For_Quarter_vw', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 1, 'SCHEMA', N'dbo', 'VIEW', N'Read_For_Quarter_vw', NULL, NULL
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW MaxOfInstallationReadDate_vw as
SELECT INSTALLATION_ID_link, Max(Meter_Read_Date) AS MaxOfMeter_Read_Date
FROM Readings 
GROUP BY INSTALLATION_ID_link;
GO
GRANT ALTER
	ON [dbo].[MaxOfInstallationReadDate_vw]
	TO [admin_role]
GO
GRANT SELECT
	ON [dbo].[MaxOfInstallationReadDate_vw]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[MaxOfInstallationReadDate_vw]
	TO [userRole]
GO
GRANT SELECT
	ON [dbo].[MaxOfInstallationReadDate_vw]
	TO [userRole]
GO
GRANT UPDATE
	ON [dbo].[MaxOfInstallationReadDate_vw]
	TO [userRole]
GO

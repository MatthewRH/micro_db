SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[AllFiTwithMaxRead_vw] as
SELECT i.INSTALLATION_ID, [FiT_ID_Initial] AS FiT_ID, i.Generation_Type, i.Generation_Capacity, 
i.Total_Installed_Capacity_kW, mi.MaxOfMeter_Read_Date
FROM INSTALLATION i LEFT JOIN MaxOfInstallationReadDate_vw mi ON i.INSTALLATION_ID = mi.INSTALLATION_ID_link;

GO
GRANT SELECT
	ON [dbo].[AllFiTwithMaxRead_vw]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[AllFiTwithMaxRead_vw]
	TO [userRole]
GO
GRANT SELECT
	ON [dbo].[AllFiTwithMaxRead_vw]
	TO [userRole]
GO
GRANT UPDATE
	ON [dbo].[AllFiTwithMaxRead_vw]
	TO [userRole]
GO

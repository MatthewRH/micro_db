SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[search_data_original] as
SELECT BP.BP_ID as BP_ID, 
CASE WHEN Company IS null  THEN 
(ISNULL(bp.Title,'_') + ' ' + ISNULL(bp.First_Name,'_') + ' ' + ISNULL(bp.Last_Name,'_'))
ELSE
company
END  AS Full_Name,
--(ISNULL(bp.Title,'_') + ' ' + ISNULL(bp.First_Name,'_') + ' ' + ISNULL(bp.Last_Name,'_')) AS Full_Name,
ISNULL(BP.GeneratorID,'_') as GeneratorID, ISNULL(BP.Address_1,'_') as Address_1, ISNULL(BP.Address_2,'_') as Address_2,
ISNULL(BP.City,'_') as City, ISNULL(BP.County,'_') as County, ISNULL(BP.Postal_Code,'_') as Postal_Code, ISNULL(BP.Email_Address,'_') as Email_Address, CT.CONTRACT_ID as CONTRACT_ID, 
ISNULL(CT.Account_Status,'_') as Account_Status, ISNULL(CT.Move_In,'1900-01-01') as Move_In, ISNULL(CT.Move_Out,'1900-01-01') as Move_Out, ISNULL(CT.Third_Party,'_') as Third_Party, ISNULL(CT.SAP_BP,0) as SAP_BP, 
ISNULL(CT.SAP_CA,0) as SAP_CA, ISNULL(i.Supply_MPAN,'_') as Supply_MPAN, i.INSTALLATION_ID as INSTALLATION_ID , ISNULL(i.Generation_Type,'_') as Generation_Type, ISNULL(i.Address_1,'_') as iAddress_1, 
ISNULL(i.Address_2,'_') as iAddress_2, ISNULL(i.City,'_') as iCity,  isnull(i.County,'_') as iCounty,
ISNULL(i.Postal_Code,'_') as iPostal_Code,   ISNULL(i.FiT_ID_Initial,'_') as  FiT_Main,
BP.Title,
BP.First_Name,
BP.Last_Name,
BP.Customer_Type,
BP.VAT_Reg_Number,
BP.Added_To_SAP,
BP.ID_Received,
BP.Business_Phone,
BP.Home_Phone,
BP.Mobile_Phone,
BP.Paperless_Customer,
CT.IMPORTANT,
BP.Company,
CT.Third_Party_Ref,
ISNULL(i.Generation_MSN,'_') as Generation_MSN
FROM (CONTRACT CT LEFT JOIN BP ON CT.BP_ID_link = BP.BP_ID) 
LEFT JOIN INSTALLATION i ON CT.INSTALLATION_ID_link = i.INSTALLATION_ID;


GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CT"
            Begin Extent = 
               Top = 0
               Left = 462
               Bottom = 335
               Right = 754
            End
            DisplayFlags = 280
            TopColumn = 8
         End
         Begin Table = "i"
            Begin Extent = 
               Top = 0
               Left = 941
               Bottom = 335
               Right = 1225
            End
            DisplayFlags = 280
            TopColumn = 53
         End
         Begin Table = "BP"
            Begin Extent = 
               Top = 0
               Left = 38
               Bottom = 335
               Right = 251
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'search_data_original', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 1, 'SCHEMA', N'dbo', 'VIEW', N'search_data_original', NULL, NULL
GO
GRANT SELECT
	ON [dbo].[search_data_original]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[search_data_original]
	TO [userRole]
GO
GRANT SELECT
	ON [dbo].[search_data_original]
	TO [userRole]
GO
GRANT UPDATE
	ON [dbo].[search_data_original]
	TO [userRole]
GO

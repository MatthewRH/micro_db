SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[MailMergeData_vw] as
SELECT BP.BP_ID as BP_ID, 
CASE WHEN Company IS null  THEN 
	(ISNULL(bp.Title,'_') + ' ' + ISNULL(bp.First_Name,'_') + ' ' + ISNULL(bp.Last_Name,'_'))
ELSE
	company
END  AS Full_Name,
--(ISNULL(bp.Title,'_') + ' ' + ISNULL(bp.First_Name,'_') + ' ' + ISNULL(bp.Last_Name,'_')) AS Full_Name,
ISNULL(BP.GeneratorID,'_') as GeneratorID, ISNULL(BP.Address_1,'_') as Address_1, ISNULL(BP.Address_2,'_') as Address_2,
ISNULL(BP.City,'_') as City, ISNULL(BP.County,'_') as County, ISNULL(BP.Postal_Code,'_') as Postal_Code, ISNULL(BP.Email_Address,'_') as Email_Address, CT.CONTRACT_ID as CONTRACT_ID, 
ISNULL(CT.Account_Status,'_') as Account_Status, ISNULL(CT.Move_In,'1900-01-01') as Move_In, ISNULL(CT.Move_Out,'1900-01-01') as Move_Out, ISNULL(CT.Third_Party,'_') as Third_Party, ISNULL(CT.SAP_BP,0) as SAP_BP, 
ISNULL(CT.SAP_CA,0) as SAP_CA, ISNULL(i.Supply_MPAN,'_') as Supply_MPAN, i.INSTALLATION_ID as INSTALLATION_ID , ISNULL(i.Generation_Type,'_') as Generation_Type, ISNULL(i.Address_1,'_') as iAddress_1, 
ISNULL(i.Address_2,'_') as iAddress_2, ISNULL(i.City,'_') as iCity,  isnull(i.County,'_') as iCounty,
ISNULL(i.Postal_Code,'_') as iPostal_Code,   ISNULL(i.FiT_ID_Initial,'_') as  FiT_Main,
BP.Title,
BP.First_Name,
BP.Last_Name,
BP.Customer_Type,
BP.VAT_Reg_Number,
BP.Added_To_SAP,
BP.ID_Received,
BP.Business_Phone,
BP.Home_Phone,
BP.Mobile_Phone,
BP.Paperless_Customer,
CT.IMPORTANT,
BP.Company,
i.Export_Status,
i.MCS_Certification_Number_1,
i.MCS_Certification_Number_2,
i.MCS_Certification_Number_3,
Ct.T_C_Sent_Out_Date_Ext1,
Ct.T_C_Sent_Out_Date_Ext2,
Ct.T_C_Agreed_Date_Ext1,
Ct.T_C_Agreed_Date_Ext2,
i.Initial_System_Tariff_Rate,
ISNULL(v1.Price,0) as Initial_Price, --(This should be taken from View created from Request ID 7533 and linked by Tariff_Code between Initial_System_Tariff_Rate.INSTALLATION & Tariff_Code.Current_Price)
i.Initial_System_Start_Date,
i.Initial_System_End_Date,
ct.Payment_Method,
i.extension_1_Tariff_Rate,
ISNULL(v2.Price,0) as Ext1_Current_Price,  --  (This should be taken from View created from Request ID 7533 and linked by Tariff_Code between Extension_1_Tariff_Rate.INSTALLATION & Tariff_Code.Current_Price)
i.Extension_1_Start_Date,
i.Extension_1_End_Date,
i.Extension_2_Tariff_Rate,
ISNULL(v3.Price,0) as Ext2_Current_Price, --    (This should be taken from View created from Request ID 7533 and linked by Tariff_Code between Extension_2_Tariff_Rate.INSTALLATION & Tariff_Code.Current_Price)
i.Extension_2_Start_Date,
i.Extension_2_End_Date,
i.Initial_System_Export_Rate,
i.Extension_1_Export_Rate,
i.Extension_2_Export_Rate
FROM (CONTRACT CT LEFT JOIN BP ON CT.BP_ID_link = BP.BP_ID) 
LEFT JOIN INSTALLATION i	  ON CT.INSTALLATION_ID_link = i.INSTALLATION_ID
LEFT JOIN rates_current_vw v1 ON i.Initial_System_Tariff_Rate = v1.tariff_code
LEFT JOIN rates_current_vw v2 ON i.Extension_1_Tariff_Rate = v2.tariff_code
LEFT JOIN rates_current_vw v3 ON i.Extension_2_Tariff_Rate = v3.tariff_code
GO
GRANT SELECT
	ON [dbo].[MailMergeData_vw]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[MailMergeData_vw]
	TO [userRole]
GO
GRANT SELECT
	ON [dbo].[MailMergeData_vw]
	TO [userRole]
GO
GRANT UPDATE
	ON [dbo].[MailMergeData_vw]
	TO [userRole]
GO

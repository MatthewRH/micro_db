SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[MailMergeData2_vw] as
SELECT BP.BP_ID as BP_ID, 
CASE WHEN Company IS null  THEN 
	(ISNULL(bp.Title,'_') + ' ' + ISNULL(bp.First_Name,'_') + ' ' + ISNULL(bp.Last_Name,'_'))
ELSE
	company
END  AS Full_Name,
--(ISNULL(bp.Title,'_') + ' ' + ISNULL(bp.First_Name,'_') + ' ' + ISNULL(bp.Last_Name,'_')) AS Full_Name,
BP.GeneratorID as GeneratorID, BP.Address_1 as Address_1, BP.Address_2 as Address_2,
BP.City as City, BP.County as County, BP.Postal_Code as Postal_Code, BP.Email_Address as Email_Address, CT.CONTRACT_ID as CONTRACT_ID, 
CT.Account_Status as Account_Status, ISNULL(CT.Move_In,'1900-01-01') as Move_In, ISNULL(CT.Move_Out,'1900-01-01') as Move_Out, CT.Third_Party as Third_Party, ISNULL(CT.SAP_BP,0) as SAP_BP, 
ISNULL(CT.SAP_CA,0) as SAP_CA, i.Supply_MPAN as Supply_MPAN, i.INSTALLATION_ID as INSTALLATION_ID , i.Generation_Type as Generation_Type, i.Address_1 as iAddress_1, 
i.Address_2 as iAddress_2, i.City as iCity,  i.County as iCounty,
i.Postal_Code as iPostal_Code,   i.FiT_ID_Initial as  FiT_Main,
BP.Title,
BP.First_Name,
BP.Last_Name,
BP.Customer_Type,
BP.ID_Received,
BP.Paperless_Customer,
BP.Company,
i.FiT_ID_Initial,
i.FiT_ID_Ext1,
i.FiT_ID_Ext2,
i.Export_Status,
i.MCS_Certification_Number_1,
i.MCS_Certification_Number_2,
i.MCS_Certification_Number_3,
Ct.T_C_Sent_Out_Date_Ext1,
Ct.T_C_Sent_Out_Date_Ext2,
Ct.T_C_Agreed_Date_Ext1,
Ct.T_C_Agreed_Date_Ext2,
i.Initial_System_Start_Date,
i.Initial_System_End_Date,
ct.Payment_Method,
i.Initial_System_Tariff_Rate,
ISNULL(v4.Price,0) as Initial_System_Current_Price, --(This should be taken from View created from Request ID 7533 and linked by Tariff_Code between Initial_System_Tariff_Rate.INSTALLATION & Tariff_Code.Current_Price)
i.extension_1_Tariff_Rate,
ISNULL(v5.Price,0) as Extension_1_Current_Price,  --  (This should be taken from View created from Request ID 7533 and linked by Tariff_Code between Extension_1_Tariff_Rate.INSTALLATION & Tariff_Code.Current_Price)
i.Extension_2_Tariff_Rate,
ISNULL(v6.Price,0) as Extension_2_Current_Price, --    (This should be taken from View created from Request ID 7533 and linked by Tariff_Code between Extension_2_Tariff_Rate.INSTALLATION & Tariff_Code.Current_Price)
i.Initial_System_Export_Rate,
ISNULL(v1.Price,0) as Initial_System_Export_Current_Price,
i.Extension_1_Export_Rate,
ISNULL(v2.Price,0) as Extension_1_Export_Current_Price,
i.Extension_2_Export_Rate,
ISNULL(v3.Price,0) as Extension_2_Export_Current_Price,
i.Extension_1_Start_Date,
i.Extension_1_End_Date,
i.Extension_2_Start_Date,
i.Extension_2_End_Date,
i.Generation_MSN,
i.RO_Accreditation_Number,
ct.Bank_Acc_Name,
ct.Sort_Code,
ct.Bank_Acc_Number,
ct.Bank_Reference,
ct.NR_Company,
ct.NR_Title,
ct.NR_First_Name,
ct.NR_Last_Name,
ct.NR_Business_Phone,
ct.NR_Home_Phone,
ct.NR_Mobile_Phone,
ct.NR_Email_Address,
ct.NR_Address_1,
ct.NR_Address_2,
ct.NR_City,
ct.NR_County,
ct.NR_Postal_Code,
ct.Terms_and_Conditions_Sent_Out_Date,
ct.Terms_and_Conditions_Agreed_Date

FROM  CONTRACT CT LEFT JOIN BP ON CT.BP_ID_link = BP.BP_ID
LEFT JOIN INSTALLATION i	  ON CT.INSTALLATION_ID_link		= i.INSTALLATION_ID
LEFT JOIN rates_current_vw v1 ON i.Initial_System_Export_Rate	= v1.tariff_code
LEFT JOIN rates_current_vw v2 ON i.Extension_1_Export_Rate		= v2.tariff_code
LEFT JOIN rates_current_vw v3 ON i.Extension_2_Export_Rate		= v3.tariff_code
LEFT JOIN rates_current_vw v4 ON i.Initial_system_Tariff_Rate	= v4.tariff_code
LEFT JOIN rates_current_vw v5 ON i.Extension_1_Tariff_Rate		= v5.tariff_code
LEFT JOIN rates_current_vw v6 ON i.Extension_2_Tariff_Rate		= v6.tariff_code


GO
GRANT SELECT
	ON [dbo].[MailMergeData2_vw]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[MailMergeData2_vw]
	TO [userRole]
GO
GRANT SELECT
	ON [dbo].[MailMergeData2_vw]
	TO [userRole]
GO
GRANT UPDATE
	ON [dbo].[MailMergeData2_vw]
	TO [userRole]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[ESP_Billing_Start_Read_vw]
AS
SELECT        Check_Start_Date, Check_End_Date, Meter_Read_Reason, INSTALLATION_ID_link, Start_MSN, Start_Read_ID, ESP_Billed, Billing_Start, Start_Read
FROM            (SELECT        

CONVERT(date, Main_Read.Meter_Read_Date) AS Check_Start_Date, 
CONVERT(date, E_1.Max_Bill_End) AS Check_End_Date, 
Main_Read.Meter_Read_Reason, 
Main_Read.INSTALLATION_ID_link, 
Main_Read.MSN AS Start_MSN, 
Main_Read.READ_ID AS Start_Read_ID, 
Main_Read.ESP_Billed, 
CASE WHEN E_1.Contract_id_link IS NOT NULL THEN Max_Bill_End
	WHEN (SELECT MIN(Meter_Read_Date) FROM Readings R_1 WHERE R_1.INSTALLATION_ID_link = Main_Read.INSTALLATION_ID_link) = Main_Read.Meter_Read_Date THEN Main_Read.Meter_Read_Date 
	WHEN (SELECT MIN(Meter_Read_Date) FROM Readings R_1 WHERE R_1.INSTALLATION_ID_link = Main_Read.INSTALLATION_ID_link AND R_1.Meter_Read_Reason = 'Initial (MX)') = Main_Read.Meter_Read_Date THEN Main_Read.Meter_Read_Date 
	WHEN (SELECT MIN(Meter_Read_Date) FROM Readings R_1 WHERE R_1.INSTALLATION_ID_link = Main_Read.INSTALLATION_ID_link AND R_1.Meter_Read_Reason = 'Initial (COT)') = Main_Read.Meter_Read_Date THEN Main_Read.Meter_Read_Date 
	ELSE NULL 
END AS Billing_Start, 
													
CASE WHEN E_1.Contract_ID_link IS NOT NULL THEN E_1.end_read 
	WHEN (SELECT MIN(Meter_Read_Date) FROM Readings R_1 WHERE R_1.INSTALLATION_ID_link = Main_Read.INSTALLATION_ID_link) = Main_Read.Meter_Read_Date THEN Main_Read.Generation_Read 
	WHEN (SELECT MIN(Meter_Read_Date) FROM Readings R_1 WHERE R_1.INSTALLATION_ID_link = Main_Read.INSTALLATION_ID_link AND R_1.Meter_Read_Reason = 'Initial (MX)') = Main_Read.Meter_Read_Date THEN Main_Read.Generation_Read 
	WHEN (SELECT MIN(Meter_Read_Date) FROM Readings R_1 WHERE R_1.INSTALLATION_ID_link = Main_Read.INSTALLATION_ID_link AND R_1.Meter_Read_Reason = 'Initial (COT)') = Main_Read.Meter_Read_Date THEN Main_Read.Generation_Read 
	ELSE NULL 
END AS Start_Read

FROM 
dbo.READINGS AS Main_Read 
INNER JOIN dbo.CONTRACT ON Main_Read.INSTALLATION_ID_link = dbo.CONTRACT.INSTALLATION_ID_link 
LEFT OUTER JOIN (Select
ESP_Invoicing.Contract_ID_Link,
Max_Bill_End,
End_Read
From ESP_Invoicing inner Join 
(Select
Contract_ID_Link,
Max(Billing_End) AS Max_Bill_End
from ESP_Invoicing
Group By Contract_ID_Link) MaxDate
on ESP_Invoicing.Contract_ID_Link = MaxDate.Contract_ID_Link and Max_Bill_End = Billing_End) AS E_1 ON E_1.Contract_ID_Link = dbo.CONTRACT.CONTRACT_ID) AS Data

WHERE 
(ESP_billed = 1) 
AND (Check_Start_Date = Billing_Start)




GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Data"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 248
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'ESP_Billing_Start_Read_vw', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 1, 'SCHEMA', N'dbo', 'VIEW', N'ESP_Billing_Start_Read_vw', NULL, NULL
GO

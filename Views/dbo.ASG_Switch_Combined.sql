SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ASG_Switch_Combined]
AS
SELECT        
FiT_ID_Initial, Address_1, Address_2, City, Postal_Code, Supply_MPAN, Account_Status, Move_In, Move_Out, Third_Party, Third_Party_Ref, Assigned_Agent, Raised_By, Raised_Date, Switch_Attempt, 
Proposed_Switch_Date, Completed_By, Completed_Date, Error_1, Error_1_Responsible_Party, Error_1_Resolved_By, Error_2, Error_2_Responsible_Party, Error_2_Resolved_By, Error_3, 
Error_3_Responsible_Party, Error_3_Resolved_By, Expiry_Reason_1, Expiry_Reason_2, Expiry_Reason_3, Expiry_Reason_4, Expiry_Reason_5, [Expiry_date], Switch_Accepted_Date, Error1_Recieved_Date, 
Error2_Recieved_Date, Error3_Recieved_Date, Assigned_Agent_To_Complete, Target_Date, IMPORTANT
FROM            contract INNER JOIN
                         INSTALLATION ON CONTRACT.INSTALLATION_ID_link = INSTALLATION.INSTALLATION_ID INNER JOIN
                         ASG_Switch ON INSTALLATION.FiT_ID_Initial = ASG_Switch.FIT_ID
UNION
SELECT        
FiT_ID_Initial, Address_1, Address_2, City, Postal_Code, Supply_MPAN, Account_Status, Move_In, Move_Out, Third_Party, Third_Party_Ref, '' AS Assigned_Agent, '' AS Raised_By, '' AS Raised_Date, 
'' AS Switch_Attempt, '' AS Proposed_Switch_Date, '' AS Completed_By, '' AS Completed_Date, '' AS Error_1, '' AS Error_1_Responsible_Party, '' AS Error_1_Resolved_By, '' AS Error_2, 
'' AS Error_2_Responsible_Party, '' AS Error_2_Resolved_By, '' AS Error_3, '' AS Error_3_Responsible_Party, '' AS Error_3_Resolved_By, '' AS Expiry_Reason_1, '' AS Expiry_Reason_2, '' AS Expiry_Reason_3, 
'' AS Expiry_Reason_4, '' AS Expiry_Reason_5, '' AS [Expiry_date], '' AS Switch_Accepted_Date, '' AS Error1_Recieved_Date, '' AS Error2_Recieved_Date, '' AS Error3_Recieved_Date, 
'' AS Assigned_Agent_To_Complete, '' AS Target_Date, IMPORTANT
FROM            contract INNER JOIN
                         installation ON contract.INSTALLATION_ID_link = INSTALLATION.INSTALLATION_ID 
						 LEFT JOIN
						 ASG_switch on INSTALLATION.FiT_ID_Initial = ASG_Switch.FIT_ID
WHERE        Account_Status LIKE '%ASG%' AND ASG_Switch.Switch_ID is null

GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'ASG_Switch_Combined', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 1, 'SCHEMA', N'dbo', 'VIEW', N'ASG_Switch_Combined', NULL, NULL
GO
GRANT SELECT
	ON [dbo].[ASG_Switch_Combined]
	TO [userRole]
GO
GRANT SELECT
	ON [dbo].[ASG_Switch_Combined]
	TO [eco\Craig.Wilkins]
GO
GRANT SELECT
	ON [dbo].[ASG_Switch_Combined]
	TO [eco\George.Fensome]
GO
GRANT SELECT
	ON [dbo].[ASG_Switch_Combined]
	TO [eco\Eve.Uzzell]
GO
GRANT SELECT
	ON [dbo].[ASG_Switch_Combined]
	TO [eco\Niall.Kimber]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[search_data]
AS
SELECT        dbo.BP.BP_ID, 

CASE WHEN Company IS NULL THEN (ISNULL(bp.Title, '_') + ' ' + ISNULL(bp.First_Name, '_') + ' ' + ISNULL(bp.Last_Name, '_'))
ELSE (ISNULL(bp.Title, '') + ' ' + ISNULL(bp.First_Name, '') + ' ' + ISNULL(bp.Last_Name, '')) +' ('+ company +')'
END AS Full_Name,

ISNULL(dbo.BP.GeneratorID, 
                         '_') AS GeneratorID, ISNULL(dbo.BP.Address_1, '_') AS Address_1, ISNULL(dbo.BP.Address_2, '_') AS Address_2, ISNULL(dbo.BP.City, '_') AS City, ISNULL(dbo.BP.County, '_') AS County, 
                         ISNULL(dbo.BP.Postal_Code, '_') AS Postal_Code, ISNULL(dbo.BP.Email_Address, '_') AS Email_Address, ISNULL(dbo.BP.Email_2, '_') AS Email_Address2, ISNULL(dbo.BP.Email_3, '_') AS Email_Address3, 
                         CT.CONTRACT_ID, ISNULL(CT.Account_Status, '_') AS Account_Status, ISNULL(CT.Move_In, '1900-01-01') AS Move_In, ISNULL(CT.Move_Out, '1900-01-01') AS Move_Out, ISNULL(CT.Third_Party, '_') 
                         AS Third_Party, ISNULL(CT.SAP_BP, 0) AS SAP_BP, ISNULL(CT.SAP_CA, 0) AS SAP_CA, ISNULL(i.Supply_MPAN, '_') AS Supply_MPAN, i.INSTALLATION_ID, ISNULL(i.Generation_Type, '_') AS Generation_Type, 
                         ISNULL(i.Address_1, '_') AS iAddress_1, ISNULL(i.Address_2, '_') AS iAddress_2, ISNULL(i.City, '_') AS iCity, ISNULL(i.County, '_') AS iCounty, ISNULL(i.Postal_Code, '_') AS iPostal_Code, ISNULL(i.FiT_ID_Initial, 
                         '_') AS FiT_Main, dbo.BP.Title, dbo.BP.First_Name, dbo.BP.Last_Name, dbo.BP.Customer_Type, dbo.BP.VAT_Reg_Number, dbo.BP.Added_To_SAP, dbo.BP.ID_Received, dbo.BP.Business_Phone, 
                         dbo.BP.Home_Phone, dbo.BP.Mobile_Phone, dbo.BP.Paperless_Customer, CT.IMPORTANT, dbo.BP.Company, CT.Third_Party_Ref, ISNULL(i.Generation_MSN, '_') AS Generation_MSN, VAT_Invoice_Manual, VAT_Invoice_Self_Billing, Meter_Read_Assistance_Required,ISNULL(E1.SAP_CA, 0) AS ESP_SAP_CA,BP.Vendor_Number

FROM            dbo.CONTRACT AS CT LEFT OUTER JOIN
                         dbo.BP ON CT.BP_ID_link = dbo.BP.BP_ID LEFT OUTER JOIN
                         dbo.INSTALLATION AS i ON CT.INSTALLATION_ID_link = i.INSTALLATION_ID LEFT OUTER JOIN
                         dbo.ESP_Contract as E1 on CT.CONTRACT_ID = E1.Contract_ID_Link

GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CT"
            Begin Extent = 
               Top = 0
               Left = 462
               Bottom = 335
               Right = 754
            End
            DisplayFlags = 280
            TopColumn = 8
         End
         Begin Table = "i"
            Begin Extent = 
               Top = 0
               Left = 941
               Bottom = 335
               Right = 1225
            End
            DisplayFlags = 280
            TopColumn = 53
         End
         Begin Table = "BP"
            Begin Extent = 
               Top = 0
               Left = 38
               Bottom = 335
               Right = 251
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'search_data', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 1, 'SCHEMA', N'dbo', 'VIEW', N'search_data', NULL, NULL
GO
GRANT SELECT
	ON [dbo].[search_data]
	TO [admin_role]
GO
GRANT INSERT
	ON [dbo].[search_data]
	TO [userRole]
GO
GRANT SELECT
	ON [dbo].[search_data]
	TO [userRole]
GO
GRANT UPDATE
	ON [dbo].[search_data]
	TO [userRole]
GO

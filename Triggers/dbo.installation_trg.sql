SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

create trigger [dbo].[installation_trg] on [dbo].[installation]
  after UPDATE
  AS
  BEGIN
	IF @@ROWCOUNT = 0 RETURN;
	UPDATE installation SET modified_date2 = GETDATE() , modified_by2 = SUSER_NAME()
	FROM installation f INNER JOIN Inserted i  on i.installation_id = f.installation_id;
  END;
GO

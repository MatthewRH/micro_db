SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[bp_trg] on dbo.BP
  after UPDATE
  AS
  BEGIN
	IF @@ROWCOUNT = 0 RETURN;
	UPDATE BP SET modified_date2 = GETDATE() , modified_by2 = SUSER_NAME()
	FROM BP f INNER JOIN Inserted i  on i.bp_id = f.bp_id;
  END;
GO

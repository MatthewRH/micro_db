SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[contract_trg] on [dbo].[contract]
  after UPDATE
  AS
  BEGIN
	IF @@ROWCOUNT = 0 RETURN;
	UPDATE contract SET modified_date2 = GETDATE() , modified_by2 = SUSER_NAME()
	FROM contract f INNER JOIN Inserted i  on i.contract_id = f.contract_id;
  END;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[readings_trg] on [dbo].[readings]
  after UPDATE
  AS
  BEGIN
	IF @@ROWCOUNT = 0 RETURN;
	UPDATE readings SET modified_date2 = GETDATE() , modified_by2 = SUSER_NAME()
	FROM readings f INNER JOIN Inserted i  on i.read_id = f.read_id;
  END;
GO

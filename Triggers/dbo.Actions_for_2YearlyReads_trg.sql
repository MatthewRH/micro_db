SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
 create trigger Actions_for_2YearlyReads_trg on dbo.Actions_for_2YearlyReads
  after UPDATE
  AS
  BEGIN
	IF @@ROWCOUNT = 0 RETURN;
	UPDATE Actions_for_2YearlyReads SET modified_date = GETDATE() , modified_by = SUSER_NAME()
	FROM Actions_for_2YearlyReads f INNER JOIN Inserted i  on i.record_id = f.record_id;
  END;
GO

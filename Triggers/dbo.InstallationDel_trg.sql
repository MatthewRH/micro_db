SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE trigger [dbo].InstallationDel_trg on [dbo].[Installation]
  after DELETE
  AS
  BEGIN
	
	IF @@ROWCOUNT = 0 RETURN;
	INSERT INTO Audit  
	select 'Installation', GETDATE() , SUSER_NAME(), 'Installation_ID', convert(varchar(10),Installation_ID) + ' - ' + Supply_MPAN + ' - ' + export_mpan + ' - ' + Fit_id_Initial
	FROM deleted
  END;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[contract_trg2]
   ON [dbo].[CONTRACT] 
   AFTER UPDATE
AS BEGIN
    SET NOCOUNT ON;
    IF UPDATE (Third_Party) 
    BEGIN
        UPDATE CONTRACT 
        SET 
		  Bank_Acc_Name = B.Bank_Acc_Name,
		  Bank_Acc_Number = B.Bank_Acc_Number,
		  Sort_Code = B.Sort_Code
        FROM CONTRACT C INNER JOIN Bank_Details B
        ON C.Third_Party = B.Third_Party  
        WHERE C.Third_Party = B.Third_Party
    END 
  END
GO

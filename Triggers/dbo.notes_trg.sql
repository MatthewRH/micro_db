SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[notes_trg] on [dbo].[notes]
  after UPDATE
  AS
  BEGIN
	IF @@ROWCOUNT = 0 RETURN;
	UPDATE notes SET modified_date2 = GETDATE() , modified_by2 = SUSER_NAME()
	FROM notes f INNER JOIN Inserted i  on i.note_id = f.note_id;
  END;
GO

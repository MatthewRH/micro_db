SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[rates_trg] on [dbo].[rates]
  after UPDATE
  AS
  BEGIN
	IF @@ROWCOUNT = 0 RETURN;
	UPDATE rates SET modified_date2 = GETDATE() , modified_by2 = SUSER_NAME()
	FROM rates f INNER JOIN Inserted i  on i.rate_id = f.rate_id;
  END;
  
GO

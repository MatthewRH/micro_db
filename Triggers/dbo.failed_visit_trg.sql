SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create trigger failed_visit_trg on dbo.failed_visits
  after UPDATE
  AS
  BEGIN
	IF @@ROWCOUNT = 0 RETURN;
	UPDATE failed_visits SET modified_date = GETDATE() , modified_by = SUSER_NAME()
	FROM failed_visits f INNER JOIN Inserted i  on i.read_id = f.read_id;
  END;
  
GO

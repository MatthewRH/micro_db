CREATE ROLE [admin_role] AUTHORIZATION [dbo]
GO

EXEC sp_addrolemember @rolename=N'admin_role', @membername =N'eco\alex.booth'

GO
EXEC sp_addrolemember @rolename=N'admin_role', @membername =N'ECO\dan.barker'

GO
EXEC sp_addrolemember @rolename=N'admin_role', @membername =N'ECO\dave.wood'

GO
EXEC sp_addrolemember @rolename=N'admin_role', @membername =N'eco\glynn.hammond'

GO
EXEC sp_addrolemember @rolename=N'admin_role', @membername =N'ECO\steve.lloyd'

GO
EXEC sp_addrolemember @rolename=N'admin_role', @membername =N'eco\Steve.Robertson'

GO

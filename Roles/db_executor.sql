CREATE ROLE [db_executor] AUTHORIZATION [dbo]
GO

EXEC sp_addrolemember @rolename=N'db_executor', @membername =N'eco\matthew.hollands'

GO

CREATE ROLE [userRole] AUTHORIZATION [dbo]
GO

ALTER ROLE [userRole] ADD MEMBER [ECO\dan.barker]

GO
ALTER ROLE [userRole] ADD MEMBER [ECO\dave.wood]

GO
ALTER ROLE [userRole] ADD MEMBER [ECO\eco-loc-sql-microtricity]

GO
ALTER ROLE [userRole] ADD MEMBER [ECO\ECO-SVC-MattHollands]

GO

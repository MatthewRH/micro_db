SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ESP_Invoice_Update] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

/* Insert normal invoices into ESP_Invoicing */
insert into ESP_Invoicing 
(Contract_ID_Link,
invoice_Number,
Contract_Account,
meter_Read_Reason,
First_Name,
Last_Name,
Email_Address,
Address,
Address_2,
Address_3,
City,County,
PostCode,
Contract_Start_Date,
Invoice_Date,
Invoice_Due_Date,
Invoice_Method,
Total_Past_Due_Charges,
Last_Payment_Date,
Last_Payment_Amount,
Billing_Start,
Billing_End,
Total_Anticipated_Output,
MPAN,
Total_Amount_Due,
Gross_Price,
Start_Read,
End_Read,
Generation_kWh,
Fit_Gen_Price,
Fit_Gen_Cost,
Fit_Exp_Price,
Fit_Exp_Cost,
Export_kWh,
Fit_Discount,
Net_Charges,
Discount,
Net_Cost,
VAT,
Gross_Cost,
Late_Fees,
Bank_Account_Number,
Payment_Method,
System_Size,
Meter_ID)

Select 
A.Contract_id_link,
A.invoice_Number,
A.Contract_Account,
ISNULL(A.Meter_Read_Reason, '') AS Meter_Read_Reason,
ISNULL(A.First_Name, '') AS First_Name,
ISNULL(A.Last_Name, '') AS Last_Name,
ISNULL(A.Email_Address, '') AS Email_Address,
ISNULL(A.Address_1, '') AS Address_1,
ISNULL(A.Address_2, '') AS Address_2,
ISNULL(A.Address_3, '') AS Address_3,
ISNULL(A.City, '') AS City,
ISNULL(A.County, '') AS County,
ISNULL(A.PostCode, '') AS PostCode,
ISNULL(A.Contract_Start_Date, '') AS Contract_Start_Date,
ISNULL(A.Invoice_Date, '') AS Invoice_Date,
ISNULL(A.Invoice_Due_Date, '') AS Invoice_Due_Date,
ISNULL(A.Invoice_Method, '') AS Invoice_Method,
ISNULL(A.Total_Past_Due_Charges, 0) AS Total_Past_Due_Charges,
ISNULL(A.Last_Payment_Date, '') AS Last_Payment_Date,
ISNULL(A.Last_Payment_Amount, 0) AS Last_Payment_Amount,
ISNULL(A.Billing_Start, '') AS Billing_Start,
ISNULL(A.Billing_End, '') AS Billing_End,
ISNULL(A.Total_Anticipated_Output, 0) AS Total_Anticipated_Output,
ISNULL(A.MPAN, 0) AS MPAN,
ISNULL(A.Total_Amount_Due, 0) AS Total_Amount_Due,
ISNULL(A.Gross_Price, 0) AS Gross_Price,
ISNULL(A.Start_Read, 0) AS Start_Read,
ISNULL(A.End_Read, 0) AS End_Read,
ISNULL(A.Generation_kWh, 0) AS Generation_kWh,
ISNULL(A.Fit_Gen_Price, 0) AS Fit_Gen_Price,
ISNULL(A.Fit_Gen_Cost, 0) AS Fit_Gen_Cost,
ISNULL(A.Fit_Exp_Price, 0) AS Fit_Exp_Price,
ISNULL(A.Fit_Exp_Cost, 0) AS Fit_Exp_Cost,
ISNULL(A.Export_kWh, 0) AS Export_kWh,
ISNULL(A.Fit_Discount, 0) AS Fit_Discount,
ISNULL(A.Net_Charges, 0) AS Net_Charges,
ISNULL(A.Discount, 0) AS Discount,
ISNULL(A.Net_Cost, 0) AS Net_Cost,
ISNULL(A.VAT, 0) AS VAT,
ISNULL(A.Gross_Cost, 0) AS Gross_Cost,
ISNULL(A.Late_Fees, 0) AS Late_Fees,
ISNULL(A.Bank_Acc_Number, 0) AS Bank_Acc_Number,
ISNULL(A.Payment_Method, '') AS Payment_Method,
ISNULL(A.System_Size, 0) AS System_Size,
ISNULL(A.Meter_ID, '') AS Meter_ID

from esp_billing A left join ESP_Invoicing B
on A.Contract_ID_Link = B.Contract_ID_Link  
Where 
A.Invoice_Number <> B.Invoice_Number and
A.Billing_Start > B.Billing_End
END


GO
GRANT EXECUTE
	ON [dbo].[ESP_Invoice_Update]
	TO [admin_role]
GO

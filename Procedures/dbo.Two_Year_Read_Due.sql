SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:           <Matthew Hollands>
-- Create date: <15/10/2015>
-- Description:      <Generates a list of Installations who have a 2 Yearly read due in the next 6 months or who are overdue for a read.  
--               It also shows any other installations which are at the same location.>
--
-- =============================================
CREATE PROCEDURE [dbo].[Two_Year_Read_Due]
AS
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;
       Declare @TwoYear Table(
GeneratorID nvarchar(255), 
BP_ID Int, 
CONTRACT_ID Int, 
TwoYear_INSTALLATION_ID Int, 
Account_Status nvarchar(255), 
TwoYear_FiT_Main nvarchar(255), 
TwoYear_Supply_MPAN nvarchar(255), 
Company nvarchar(255), 
Title nvarchar(255), 
First_Name nvarchar(255), 
Last_Name nvarchar(255), 
Third_Party nvarchar(255), 
iAddress_1 nvarchar(255), 
iAddress_2 nvarchar(255), 
iCity nvarchar(255), 
iCounty nvarchar(255), 
iPostal_Code nvarchar(255), 
Business_Phone nvarchar(255), 
Home_Phone nvarchar(255), 
Mobile_Phone nvarchar(255), 
Move_In datetime,
TwoYear_Confirmation_Date datetime, 
TwoYear_Initial_System_Start_Date datetime, 
TwoYear_Generation_Type nvarchar(255), 
TwoYear_Generation_MSN nvarchar(255), 
Two_Year_Read_Date_Original datetime,
Latest_2_Year_Read datetime,
Two_Year_Anniversary datetime,
CheckDate nvarchar(255))

INSERT INTO @TwoYear

SELECT 
MailMergeData_vw.GeneratorID, 
MailMergeData_vw.BP_ID, 
MailMergeData_vw.CONTRACT_ID, 
MailMergeData_vw.INSTALLATION_ID, 
MailMergeData_vw.Account_Status, 
MailMergeData_vw.FiT_Main, 
MailMergeData_vw.Supply_MPAN, 
MailMergeData_vw.Company, 
MailMergeData_vw.Title, 
MailMergeData_vw.First_Name, 
MailMergeData_vw.Last_Name, 
MailMergeData_vw.Third_Party, 
MailMergeData_vw.iAddress_1, 
MailMergeData_vw.iAddress_2, 
MailMergeData_vw.iCity, 
MailMergeData_vw.iCounty, 
MailMergeData_vw.iPostal_Code, 
MailMergeData_vw.Business_Phone, 
MailMergeData_vw.Home_Phone, 
MailMergeData_vw.Mobile_Phone, 
MailMergeData_vw.Move_In,
INSTALLATION.Confirmation_Date, 
MailMergeData_vw.Initial_System_Start_Date, 
MailMergeData_vw.Generation_Type, 
INSTALLATION.Generation_MSN, 
CASE WHEN Installation.Confirmation_Date = '9999-12-31 00:00:00.000' THEN NULL
ELSE DATEADD(day,730,Confirmation_Date)
END AS Two_Year_Read_Date_Original,
MaxOfMeter_Read_Date AS Latest_2_Year_Read,
CASE WHEN Installation.Confirmation_Date = '9999-12-31 00:00:00.000' THEN NULL
WHEN [MaxOfMeter_Read_Date] Is Null THEN DATEADD(day,730,Confirmation_Date)
ELSE DATEADD(day,730,MaxOfMeter_Read_Date)
END AS Two_Year_Anniversary,
CASE 
WHEN MaxOfMeter_Read_Date IS NULL AND Installation.Confirmation_Date <> '9999-12-31 00:00:00.000' AND DATEADD(day,730,INSTALLATION.Confirmation_Date) < GETDATE() THEN 'Overdue'
WHEN MaxOfMeter_Read_Date IS NOT NULL AND DATEADD(day,730,MaxOfMeter_Read_Date) < GETDATE() THEN 'Overdue'
WHEN MaxOfMeter_Read_Date IS NULL AND Installation.Confirmation_Date <> '9999-12-31 00:00:00.000' AND (DATEADD(day,730,INSTALLATION.Confirmation_Date) - GETDATE()) < 183 THEN 'Less than 6 months'
WHEN MaxOfMeter_Read_Date IS NOT NULL AND (DATEADD(day,730,MaxOfMeter_Read_Date)- GETDATE()) < 183 THEN 'Less than 6 months'
ELSE Null
END AS CheckDate

FROM 
(SELECT 
READINGS.INSTALLATION_ID_link, 
Max(READINGS.Meter_Read_Date) AS MaxOfMeter_Read_Date, 
READINGS.Meter_Read_Reason
FROM READINGS
GROUP BY READINGS.INSTALLATION_ID_link, READINGS.Meter_Read_Reason
HAVING (((READINGS.Meter_Read_Reason)='2 Year Audit'))) Latest_2_Yearly_Read
 
RIGHT JOIN (MailMergeData_vw LEFT JOIN INSTALLATION ON MailMergeData_vw.INSTALLATION_ID = INSTALLATION.INSTALLATION_ID) 
ON Latest_2_Yearly_Read.INSTALLATION_ID_link = MailMergeData_vw.INSTALLATION_ID

Select  
GeneratorID, 
BP_ID, 
CONTRACT_ID, 
TwoYear_INSTALLATION_ID, 
Account_Status, 
TwoYear_FiT_Main, 
TwoYear_Supply_MPAN, 
Company, 
Title, 
First_Name, 
Last_Name, 
Third_Party,
iAddress_1,
iAddress_2,
iCity,
iCounty,
iPostal_Code,
Business_Phone,
Home_Phone,
Mobile_Phone,
Move_In,
TwoYear_Confirmation_Date,
TwoYear_Initial_System_Start_Date,
TwoYear_Generation_Type,
TwoYear_Generation_MSN,
Two_Year_Read_Date_Original,
Latest_2_Year_Read,
Two_Year_Anniversary,
CheckDate,
CASE WHEN INSTALLATION.FiT_ID_Initial <> TwoYear_FiT_Main THEN INSTALLATION.FiT_ID_Initial
ELSE NULL
END As 'Other_Fit_at_Site',
INSTALLATION.Address_1,
INSTALLATION.Postal_Code

from @TwoYear Inner Join Installation on
iPostal_Code = INSTALLATION.Postal_Code AND 
iAddress_1 = INSTALLATION.Address_1
where CheckDate is not null
AND (Account_Status='Registered Live' Or Account_Status='Suspended-2 Yr Read')
AND GeneratorID NOT IN ('GEN5194883', 'GEN5000004', 'GEN5101894','GEN5098299','GEN5316411','GEN5194767','GEN5194792','GEN5010924','GEN5094951',
'GEN5210437','GEN5146654','GEN5150741','GEN5165811','GEN5169702','GEN5170330','GEN5197421','GEN5203246','GEN5233599','GEN5310077')
END
GO
GRANT EXECUTE
	ON [dbo].[Two_Year_Read_Due]
	TO [userRole]
GO
